﻿using System;
using System.Windows.Forms;

namespace SolfWareRegister
{
    public partial class RegForm : Form
    {
        public RegForm()
        {
            InitializeComponent();
            Application.Idle +=Application_Idle;
        }
        bool bFirstShow = true;
        private void Application_Idle(object sender, EventArgs e)
        {
            if (bFirstShow)
            {
                bFirstShow = false;
                txtLocalSerial.Text = (Computer.CurrentIns.AuthSerialID + Computer.CurrentIns.PostFix).GetHashCode().ToString();
            }
            
        }
        private void btnRegiste_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (txtLocalSerial.Text != "")
            {
                txtRegID.Text = RegHelper.GenerateSerial(txtLocalSerial.Text);
                txtExpireCode.Text = RegHelper.GenerateExpireCode(dtpExpireTime.Value);
            }
            else
            {
                errorProvider1.SetError(txtLocalSerial, "序列号不能为空");
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelfSerialID_Click(object sender, EventArgs e)
        {
            txtLocalSerial.Text = (Computer.CurrentIns.AuthSerialID + Computer.CurrentIns.PostFix).GetHashCode().ToString();
        }

        private void txtExpireCode_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtExpireCode.Text)) return;
            string strDate = txtExpireCode.Text;
            dtpExpireTime.Value = strDate.DESstringToDateTime();
        }
    }
}
