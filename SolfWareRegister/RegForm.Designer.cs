﻿namespace SolfWareRegister
{
    partial class RegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLocalSerial = new System.Windows.Forms.TextBox();
            this.txtRegID = new System.Windows.Forms.TextBox();
            this.btnRegiste = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnSelfSerialID = new System.Windows.Forms.Button();
            this.dtpExpireTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtExpireCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "本机序列号:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "注册码:";
            // 
            // txtLocalSerial
            // 
            this.txtLocalSerial.BackColor = System.Drawing.Color.White;
            this.txtLocalSerial.Location = new System.Drawing.Point(89, 39);
            this.txtLocalSerial.Name = "txtLocalSerial";
            this.txtLocalSerial.Size = new System.Drawing.Size(151, 21);
            this.txtLocalSerial.TabIndex = 2;
            // 
            // txtRegID
            // 
            this.txtRegID.BackColor = System.Drawing.Color.LightYellow;
            this.txtRegID.Location = new System.Drawing.Point(89, 97);
            this.txtRegID.Multiline = true;
            this.txtRegID.Name = "txtRegID";
            this.txtRegID.Size = new System.Drawing.Size(190, 43);
            this.txtRegID.TabIndex = 3;
            // 
            // btnRegiste
            // 
            this.btnRegiste.Location = new System.Drawing.Point(52, 264);
            this.btnRegiste.Name = "btnRegiste";
            this.btnRegiste.Size = new System.Drawing.Size(89, 26);
            this.btnRegiste.TabIndex = 4;
            this.btnRegiste.Text = "生成注册码";
            this.btnRegiste.UseVisualStyleBackColor = true;
            this.btnRegiste.Click += new System.EventHandler(this.btnRegiste_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(170, 264);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 26);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "关  闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnSelfSerialID
            // 
            this.btnSelfSerialID.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSelfSerialID.Location = new System.Drawing.Point(240, 36);
            this.btnSelfSerialID.Name = "btnSelfSerialID";
            this.btnSelfSerialID.Size = new System.Drawing.Size(39, 26);
            this.btnSelfSerialID.TabIndex = 6;
            this.btnSelfSerialID.Text = "本机";
            this.btnSelfSerialID.UseVisualStyleBackColor = true;
            this.btnSelfSerialID.Click += new System.EventHandler(this.btnSelfSerialID_Click);
            // 
            // dtpExpireTime
            // 
            this.dtpExpireTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpExpireTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpireTime.Location = new System.Drawing.Point(89, 157);
            this.dtpExpireTime.Name = "dtpExpireTime";
            this.dtpExpireTime.Size = new System.Drawing.Size(165, 21);
            this.dtpExpireTime.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "过期时间:";
            // 
            // txtExpireCode
            // 
            this.txtExpireCode.BackColor = System.Drawing.Color.LightYellow;
            this.txtExpireCode.Location = new System.Drawing.Point(89, 189);
            this.txtExpireCode.Multiline = true;
            this.txtExpireCode.Name = "txtExpireCode";
            this.txtExpireCode.Size = new System.Drawing.Size(190, 43);
            this.txtExpireCode.TabIndex = 9;
            this.txtExpireCode.TextChanged += new System.EventHandler(this.txtExpireCode_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "附加码:";
            // 
            // RegForm
            // 
            this.AcceptButton = this.btnRegiste;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(342, 316);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtExpireCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpExpireTime);
            this.Controls.Add(this.btnSelfSerialID);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnRegiste);
            this.Controls.Add(this.txtRegID);
            this.Controls.Add(this.txtLocalSerial);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RegForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "软件注册";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLocalSerial;
        private System.Windows.Forms.TextBox txtRegID;
        private System.Windows.Forms.Button btnRegiste;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnSelfSerialID;
        private System.Windows.Forms.DateTimePicker dtpExpireTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtExpireCode;
        private System.Windows.Forms.Label label4;
    }
}