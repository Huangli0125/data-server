﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SolfWareRegister
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RegForm());
        }
        private static  float BackMValue(byte[] buffer, int startBit)
        {
            float fVal;

            byte byHi = buffer[startBit];
            byte byLo = buffer[startBit + 1];
            if ((byHi & 0x80) > 0) //负数
            {
                byte[] temp = new[] { (byte)(byHi & 0x7F), byLo };
                ushort val = BitConverter.ToUInt16(temp, 0);
                val ^= 0xffff;
                val += 1;
                fVal = (short)val; //-32768--0
            }
            else //正数：原码
            {
                fVal = BitConverter.ToUInt16(buffer, startBit);
            }
            return fVal;
        }
    }
}
