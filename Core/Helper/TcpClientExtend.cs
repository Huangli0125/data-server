﻿using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Core.Helper
{
    public static class TcpClientExtend
    {
        public static TcpState GetState(this TcpClient tcpClient)
        {
            try
            {
                var foo = IPGlobalProperties.GetIPGlobalProperties()
                    .GetActiveTcpConnections()
                    .SingleOrDefault(x => x.LocalEndPoint.Equals(tcpClient.Client.LocalEndPoint)
                                          && x.RemoteEndPoint.Equals(tcpClient.Client.RemoteEndPoint)
                    );

                return foo?.State ?? TcpState.Unknown;
            }
            catch 
            {
                return TcpState.Unknown;
            }
        }
    }
}
