﻿using System;

namespace Core.Helper
{
    public static class DateHelper
    {
        /// <summary>
        /// 取得指定月的最后一天
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public  static DateTime LastDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddMonths(1).AddDays(-1);
        }
        /// <summary>
        /// 取得上月的最后一天
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime LastDayOfPrdviousMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddDays(-1);
        }
    }
}
