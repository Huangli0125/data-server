﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Core.Model;

namespace Core
{
    [ServiceContract(CallbackContract = typeof(IPushServiceCallback))]
    public interface IPushService
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe(SubscribeArg a);

        [OperationContract(IsOneWay = true)]
        void Unsubscribe(SubscribeArg a);

        [OperationContract]
        DateTime Ping();

    }

    public interface IPushServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        // ReSharper disable once OperationContractWithoutServiceContract
        void OnEventReceived(ArgumentBase<Event> evt);
    }



    #region  实体类
    [Serializable]
    public class ArgumentBase<T>
    {
        
        private int code;
        private string msg;
        private string userName;
        private T model;

        public int Code
        {
            get { return code; }
            set { code = value; }
        }
        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }
        public String Username
        {
            get { return userName; }
            set { userName = value; }
        }
        public T Model
        {
            get { return model; }
            set { model = value; }

        }
    }

    public class SubscribeArg : ArgumentBase<int>
    {
        public List<int> Alarms { get; set; }
        public SubscribeArg()
        {
            Alarms = new List<int>();
        }
    }

    public class SubscribeContext
    {
        public SubscribeArg Arg { get; set; }
        public IPushServiceCallback Callback { get; set; }

    }
    #endregion
}
