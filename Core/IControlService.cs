﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Core.Model;
using Core.Msg;

namespace Core
{
    [ServiceContract]
    public interface IControlService
    {
        #region 通道、设备、数据项操作

        [OperationContract]
        [WebGet(UriTemplate = "channel/all", ResponseFormat = WebMessageFormat.Json)] // 必须以WebGet配置，如果以WebInvoke 配置服务没法发现
        IEnumerable<Channel> GetAllChannel();

        [OperationContract]
        [WebGet(UriTemplate = "channel?id={id}", ResponseFormat = WebMessageFormat.Json)] // 必须以WebGet配置，如果以WebInvoke 配置服务没法发现
        Channel GetChannel(string id);

        [OperationContract]
        [WebGet(UriTemplate = "device?chlid={chlid}", ResponseFormat = WebMessageFormat.Json)]
        IEnumerable<Device> GetDeviceByChannelId(string chlid);

        [OperationContract]
        [WebGet(UriTemplate = "device/devid?devid={devid}", ResponseFormat = WebMessageFormat.Json)] 
        Device GetDevice(string devid);

        [OperationContract]
        [WebGet(UriTemplate = "dataitems/?devid={devid}&storage={storageFilter}", ResponseFormat = WebMessageFormat.Json)]
        IEnumerable<DataItem> GetDataItemByDeviceId(string devid,bool storageFilter);

        [OperationContract]
        [WebGet(UriTemplate = "dataitem?dataid={dataid}", ResponseFormat = WebMessageFormat.Json)]
        DataItem GetDataItem(string dataid);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        DataItem GetDataItemByRcId(string rcid);

        [OperationContract]
        [WebGet(UriTemplate = "device/type/all", ResponseFormat = WebMessageFormat.Json)] 
        IEnumerable<DeviceType> GetAllDeviceType();

        [OperationContract]
        [WebGet(UriTemplate = "device/type/operation?id={id}", ResponseFormat = WebMessageFormat.Json)] 
        IEnumerable<Operation> GetOperationByDeviceType(string id);

        [OperationContract]
        IEnumerable<Device> GetDeviceByIds(string devIds);
        [OperationContract]
        IEnumerable<DeviceCategory> GetAllCategories();

        [OperationContract]
        IEnumerable<DeviceCategory> GetCatgoryWithDevices();

        [OperationContract]
        bool SaveCategroy(IEnumerable<DeviceCategory> categories);

        [OperationContract]
        List<DataItem> GetReportDataByDevIds(string devIds);

        #endregion

        #region 遥控、命令操作 

        [OperationContract]
        [WebGet(UriTemplate = "command/excute?chlid={chlid}&devid={devid}&operid={operid}&paras={paras}&timeout={timeout}", ResponseFormat = WebMessageFormat.Json)]
        OperateResult ExcuteCommand(string chlid, string devid, string operid, string paras,int timeout);

        [OperationContract]
        [WebGet(UriTemplate = "remote/command?rcId={rcId}&on={on}&timeout={timeout}", ResponseFormat = WebMessageFormat.Json)]
        OperateResult RemoteControlCommand(string rcId, bool on, int timeout);

        [OperationContract]
        [WebGet(UriTemplate = "update/alarmwin?rcId={rcId}&on={on}",  ResponseFormat = WebMessageFormat.Json)]
        bool UpdateAlarmWin(string rcId, bool on);

        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<RemoteItem> GetRemoteItems();


        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        RemoteItem GetRemoteItem(string id);

        #endregion

        #region 通道方法调用
        [OperationContract]
        [WebGet(UriTemplate = "method/get?chlid={chlid}", ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, List<string>> GetChannelMethods(string chlid);

        [OperationContract]
        [WebGet(UriTemplate = "method/invoke?chlid={chlid}&method={method}&paras={paras}&timeout={timeout}", ResponseFormat = WebMessageFormat.Json)]
        OperateResult InvokeChannelMethod(string chlid, string method, string paras, int timeout);

        #endregion

        #region 用户管理

        [OperationContract]
        [WebGet(UriTemplate = "user/all", ResponseFormat = WebMessageFormat.Json)]
        IEnumerable<User> GetAllUsers();

        [OperationContract]
        [WebGet(UriTemplate = "user/login?name={name}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        OperateResult Login(string name, string password);

        [OperationContract]
        [WebGet(UriTemplate = "user/logout?name={name}", ResponseFormat = WebMessageFormat.Json)]
        OperateResult Logout(string name);

        [OperationContract]
        OperateResult UpdateUser(User user);
        [OperationContract]
        OperateResult RegisterUser(User user);
        [OperationContract]
        OperateResult DeleteUser(User user);
        [OperationContract]
        List<Right> GetAllRights();
        [OperationContract]
        List<Right> GetUserRights(string userId);
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool UpdateUserRights(string userId,string rights);

        [OperationContract]
        List<string> GetUserDevices(string userId);
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool UpdateUserDevices(string userId, string deviceIds);
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool UserOwnDevice(string userId, string devId);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<OperRecord> GetUserOperation(string user, string from = "", string to = "");
        [OperationContract]
        bool AddUserOperation(OperRecord record);

        #endregion

        #region 获取历史数据

        [OperationContract]
        [WebGet(UriTemplate = "his/minute/getlatest?num={num}&minute={minute}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetMinuteLatestHisData(int num, int minute, string ids,string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/minute/get?from={dateFrom}&to={dateTo}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetMinuteHisData(string dateFrom, string dateTo, string ids, string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/minute/getone?date={date}&ids={ids}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetMinuteHisData1(string date, string ids);

        [OperationContract]
        [WebGet(UriTemplate = "his/hour/getlatest?num={num}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetHourLatestHisData(int num, string ids, string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/hour/get?date={date}&num={num}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetHourHisData(string date,int num, string ids, string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/day/get?date={date}&num={num}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetDayHisData(string date,int num, string ids, string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/month/get?date={date}&num={num}&ids={ids}&format={dateFormat}", ResponseFormat = WebMessageFormat.Json)]
        List<HisDataMsg> GetMonthHisData(string date,int num, string ids, string dateFormat);

        [OperationContract]
        [WebGet(UriTemplate = "his/stat/report?type={type}&date={date}&ids={ids}", ResponseFormat = WebMessageFormat.Json)]
        List<HisStatData> GetStatisticReport(int type,string date, string ids);

        [OperationContract]
        [WebGet(UriTemplate = "his/cmp/data?type={type}&date={date}&ids={ids}", ResponseFormat = WebMessageFormat.Json)]
        List<HisCmpData> GetCmpReport(int type, string date, string ids);
        #endregion

        #region 获取/修改实时数据

        [OperationContract]
        [WebGet(UriTemplate = "realtimedata/get?ids={ids}", ResponseFormat = WebMessageFormat.Json)]
        List<RealTimeData> GetRealTimeData(string ids);

        //[WebGet(UriTemplate = "realtimedata/set?chlId={chlId}&devId={devId}&dataId={dataId}&value={value}&timeout={timeout}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped)]
        OperateResult ModifyData(string devId, string dataId, double value, int timeout);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped)]
        OperateResult ModifyDataEx(string chlId, string devId, string dataId, double value, int timeout);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped)]
        RxTxCommand DirectSendstrCommand(string chlId, string devId, string content, int timeout);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped)]
        RxTxCommand DirectSendbyteCommand(string chlId, string devId, byte[] content, int timeout);

        #endregion

        #region 页面、图片、脚本信息
        // 页面
        [OperationContract]
        List<PageInfo> GetAllPages();
        [OperationContract]
        PageInfo GetPageById(string id);
        [OperationContract]
        PageInfo GetHomePage();
        [OperationContract]
        bool SetHomePage(string pageId);
        [OperationContract]
        PageInfo GetPageByName(string name);
        [OperationContract]
        bool SavePages(List<PageInfo> pageInfos);
        [OperationContract]
        bool SavePage(PageInfo pageInfo);
        [OperationContract]
        bool DeletePage(PageInfo pageInfo);
        [OperationContract]
        bool DeletePageById(string pageId);

        // 图片
        [OperationContract]
        List<PageResource> GetAllResource();
        [OperationContract]
        bool UploadResource(PageResource resource);
        [OperationContract]
        bool UploadResources(List<PageResource> resources);
        [OperationContract]
        PageResource DownLoadResource(string id);
        [OperationContract]
        bool DeleteResource(string id);

        // 脚本
        [OperationContract]
        List<ScriptInfo> GetAllScripts();
        [OperationContract]
        List<ScriptInfo> GetGlobalScripts();
        [OperationContract]
        ScriptInfo GetScriptInfo(string scriptId);
        [OperationContract]
        ScriptInfo GetScriptInfoByPageId(string pageId);
        [OperationContract]
        bool SaveScripts(List<ScriptInfo> scriptInfos);
        [OperationContract]
        bool SaveScript(ScriptInfo scriptInfo);
        [OperationContract]
        bool DeleteScript(ScriptInfo scriptInfo);
        [OperationContract]
        bool DeleteScriptById(string id);
        [OperationContract]
        bool DeleteScriptByPageId(string pageId);
        #endregion

        #region 事件处理

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Event> GetEventsWithConfirm(EventType type, EventLevel level, bool isConfirm, string from="", string to="");
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Event> GetEvents(EventType type, EventLevel level, string from = "", string to = "");
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ConfirmEvent(string ids, string man);

        #endregion
    }
}
