﻿using System.Runtime.Serialization;

namespace Core.Msg
{
    [DataContract]
    public enum MsgResult
    {
        [EnumMember]
        SendOk =0,
        [EnumMember]
        SendFail,
        [EnumMember]
        SendTimeout,
        [EnumMember]
        RecOk,
        [EnumMember]
        RecErr,
        [EnumMember]
        RecTimeout,
        [EnumMember]
        Undefined
    }
}
