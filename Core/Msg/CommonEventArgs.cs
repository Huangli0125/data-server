﻿using System;

namespace Core.Msg
{
    public delegate void CommonHandler<T>(object sender, CommonEventArgs<T> e);
    public class CommonEventArgs<T> : EventArgs
    {
        public CommonEventArgs(T result)
        {
            Result = result;
        }
        public T Result { get; set; }
    }
}
