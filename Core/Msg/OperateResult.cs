﻿using System;
using System.Runtime.Serialization;

namespace Core.Msg
{
    [DataContract]
    public class OperateResult
    {
        [DataMember]
        public eResult Result { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Object Content { get; set; }
        public OperateResult()
        {
        }
        public OperateResult(eResult result)
        {
            Result = result;
        }
        public OperateResult(eResult result, string description)
        {
            Result = result;
            Description = description;
        }
        public OperateResult(eResult result, string description,Object obj)
        {
            Result = result;
            Description = description;
            Content = obj;
        }
    }
    [DataContract]
    public enum eResult
    {
        [EnumMember]
        S_UNKNOW,
        [EnumMember]
        S_OK,
        [EnumMember]
        S_INVALID,
        [EnumMember]
        S_NOT_FOUND,
        [EnumMember]
        S_EXCEPTION,
        [EnumMember]
        S_FAILED
    }
}
