﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

namespace Core.Msg
{
    [DataContract]
    public class RxTxCommand : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region 基本属性
        [DataMember]
        public string Name { get; set; }
        public bool IsAscii { get; set; } = false;
        public bool NeedReply { get; set; } = false;
        public bool IsInsertFrame { get; set; } = false;
        [DataMember]
        public string ChannelId { get; set; }
        [DataMember]
        public string DeviceId { get; set; }
        public int Timeout { get; set; } = 100;
        [DataMember]
        public MsgResult SendResult { get; set; } = MsgResult.Undefined;
        public DateTime _sendTime;
        public DateTime SendTime
        {
            get => _sendTime;
            set
            {
                _sendTime = value;
                NotifyPropertyChanged("SendTime");
            }
        }
        private MsgResult _receiveResult = MsgResult.Undefined;
        [DataMember]
        public MsgResult ReceiveResult
        {
            get => _receiveResult;
            set
            {
                _receiveResult = value;
                NotifyPropertyChanged("ReceiveResult");
            }
        }
        private DateTime _recTime;
        [DataMember]
        public DateTime RecTime
        {
            get => _recTime;
            set
            {
                _recTime = value;
                NotifyPropertyChanged("RecTime");
            }
        }
        #endregion

        #region 发送接收缓存
        protected byte[] _sendBytes;
        [DataMember]
        public virtual byte[] SendBytes
        {
            get => _sendBytes;
            set => _sendBytes = value;
        }

        protected byte[] _receiveBytes;
        /// <summary>
        /// 赋值后自动调用DealReceiveMsg
        /// </summary>
        [DataMember]
        public virtual byte[] ReceiveBytes
        {
            get => _receiveBytes;
            set
            {
                _receiveBytes = value;
                if(_receiveBytes !=null && _receiveBytes.Length>0)
                    ReceiveResult = MsgResult.RecOk;
                RecTime = DateTime.Now;
                DealReceiveMsg?.Invoke(this, EventArgs.Empty);
                if (PropertyChanged != null)
                {
                    NotifyPropertyChanged("RecTime");
                    NotifyPropertyChanged("ReceiveMsg");
                }
            }
        }

        private string _recContent;
        /// <summary>
        /// 返回给发命令端的结果(具体协议中处理)
        /// </summary>
        [DataMember]
        public string ReceiveContent
        {
            get
            {
                if (!string.IsNullOrEmpty(_recContent))
                    return _recContent;
                return ReceiveMsg;
            }
            set => _recContent = value;
        }

        /// <summary>
        /// 接收的报文
        /// </summary>
        public string ReceiveMsg
        {
            get
            {
                StringBuilder stringBuilder =new StringBuilder();
                try
                {
                    if (IsAscii)
                    {
                        if(_receiveBytes!=null&& _receiveBytes.Length>0)
                            stringBuilder.Append(Encoding.ASCII.GetString(_receiveBytes));
                    }
                    else
                    {
                        foreach (var item in _receiveBytes)
                        {
                            stringBuilder.Append(item.ToString("X2"));
                            stringBuilder.Append(" ");
                        }
                    }
                }
                catch 
                {
                   
                }
                return stringBuilder.ToString();
            }
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// 处理接收报文
        /// </summary>
        public  EventHandler DealReceiveMsg;

        public void RaiseReceiveEvent(Object content)
        {
            try
            {
                DealReceiveMsg?.Invoke(content, EventArgs.Empty);
            }
            catch 
            {
            }
        }
        #endregion

        #region 构造函数
        public RxTxCommand()
        {
          
        }
        public RxTxCommand(string channelId,string deviceId, string name = "普通报文")
        {
            ChannelId = channelId;
            DeviceId = deviceId;
            Name = name;
        }

        public RxTxCommand(RxTxCommand cmd)
        {
            this.Name = cmd.Name;
            this.ChannelId = cmd.ChannelId;
            this.DeviceId = cmd.DeviceId;
            this.IsAscii = cmd.IsAscii;
            this.IsInsertFrame = cmd.IsInsertFrame;
            this.NeedReply = cmd.NeedReply;
            this.RecTime = cmd.RecTime;
            this.SendTime = cmd.SendTime;
            this._receiveBytes = cmd.ReceiveBytes;
            this.ReceiveResult = cmd.ReceiveResult;
            this._sendBytes = cmd.SendBytes;
            this.SendResult = cmd.SendResult;
        }
        #endregion
    }
}
