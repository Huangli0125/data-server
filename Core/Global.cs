﻿using Core.Interface;

namespace Core
{
    public static class Global
    {
        public static IDataAccess DataAccess { get; set; }
        public static INetAccess NetAccess { get; set; }

    }
}
