﻿using System;
using System.Collections.Generic;
using Core.Model;
using Core.Model.Transmit;
using Core.Msg;

namespace Core.Interface
{
    public interface IDataAccess
    {
       
        event EventHandler.DataValueChanged OnDataValueChanged;
       
        string GetDevProtocolPath();
        string GetChlProtocolPath();
        
        List<Channel> GetAllChannels();
        Channel GetChannel(string id);
        List<Device> GetDevices(string chlId);
        List<DataItem> GetDataItems(string devId);
        RemoteItem GetRemoteItem(string id);
        void PostData(string itemId, bool privilege, object value, Int32 triggerVal = 1);
        void ValueChanged(string itemId, object value, Int32 triggerVal = 1);
        OperateResult ModifyDataValue(string chlId, string devId, string dataId, double value, int timeout);
        bool SaveDataItems(string devId, List<DataItem> dataItems);
        List<Operation> GetOperations(string devTypeId);
        bool AddEvent(EventType type, EventLevel level, string description);
        T GetValueFromIni<T>(string section, string key, T defValue);

        string GetTransmitDevProtocolPath();
        string GetTransmitChlProtocolPath();
        List<TransmitChannel> GetAllTransmitChannels();
        TransmitChannel GetTransmitChannel(string id);
        List<TransmitDevice> GetTransmitDevices(string chlId);
        List<TransmitData> GetTransmitData(string devId);
        bool SaveTransmitDatas(string devId, List<TransmitData> dataItems);
        bool ShowMessage(string channelId, string deviceId);

    }
}
