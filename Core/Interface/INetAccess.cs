﻿namespace Core.Interface
{
    public interface INetAccess
    {
        void SendWebsocketMsg(string key, object obj);
        bool IsChannelConnected(string id);
        bool IsChannelRunning(string id);
    }
}
