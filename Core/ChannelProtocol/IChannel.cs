﻿using System;
using System.Collections.Generic;
using Core.Model;
using Core.Msg;

namespace Core.ChannelProtocol
{
    public interface IChannel
    {
        event EventHandler OnNewMessage;
        Channel ChlInfo { get; set; }
        bool Connected { get; }
        
        bool Start();
        void Stop();
        void PutCommand(RxTxCommand cmd);
        void PutInsertCommand(RxTxCommand cmd);
        RxTxCommand PostToExcute(string devId, string operId);
        RxTxCommand PostToExcute(string devId, string operId,string[] paras);
        RxTxCommand RemoteControl(string devId, string rcId, bool on);
        Dictionary<string, List<string>> GetExportMethods();
        string GetMethodParas(string method);
        object InvokeMethod(string method, string paras);
    }

    /// <summary>
    /// 标记导出的方法
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ExportAttribute : Attribute
    {
        public string Description { get; set; } = "";

        public ExportAttribute()
        {
            
        }
        public ExportAttribute(string description)
        {
            Description = description;
        }
    }
}
