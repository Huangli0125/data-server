﻿namespace Core.Modbus
{
    public class ModbusInfo
    {
        /// <summary>
        /// 设备地址
        /// </summary>
        public byte DevAddr;
        /// <summary>
        /// 功能码
        /// </summary>
        public ModbusCode FunCode;
        /// <summary>
        /// 起始寄存器地址
        /// </summary>
        public ushort RegAddr;
        /// <summary>
        /// 读写寄存器个数
        /// </summary>
        public ushort RegNum;
        /// <summary>
        /// 写数据（读取时不需赋值）
        /// </summary>
        public byte[] WriteData;
        /// <summary>
        /// 发送数据包
        /// </summary>
        public byte[] SendADU;
        /// <summary>
        /// 接收数据包（外部赋值）
        /// </summary>
        public byte[] RecADU;
        /// <summary>
        /// 对应序号(内部Modbus使用)
        /// </summary>
        public int startIndex;
        public ModbusInfo()
        {
            startIndex = 0;
            DevAddr = 1;
            FunCode = ModbusCode.F03;
            RegAddr = 0x0000;
            RegNum = 0x0001;
            WriteData = null;
            SendADU = null;
            RecADU = null;
        }
    }

    public enum ModbusCode
    {
        F01=0x01,
        F02=0x02,
        F03=0x03,
        F04=0x04,
        F05=0x05,
        F06=0x06,
        F10=0x10

        //ReadCoils = 0x01,
        //ReadDisreteInput = 0x02,
        //ReadHoldingReg = 0x03,
        //ReadInputReg = 0x04,
        //WriteSingleCoil = 0x05,
        //WriteSingleReg = 0x06,
        //WriteMultipleReg = 0x10
    }

}
