﻿using System.ComponentModel;
using Core.Model;
using Newtonsoft.Json;

namespace Core.Modbus
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ModbusTagObject:Extra
    {
        private ModbusCode _readFun = ModbusCode.F03;
        private ModbusCode _writeFun = ModbusCode.F10;
        private double _coefficient = 1.0;
        [DisplayName("读功能码")]
        [JsonProperty]
        public ModbusCode ReadFunCode
        {
            get => _readFun;
            set => _readFun = value;
        }
        [DisplayName("写功能码")]
        [JsonProperty]
        public ModbusCode WriteFunCode
        {
            get => _writeFun;
            set => _writeFun = value;
        }
        [DisplayName("接收字节")]
        [Browsable(false)]
        public byte[] RecBytes { get; set; } = new byte[8];

        [Browsable(false)]
        public eAlarmType CurAlarmType { get; set; } = eAlarmType.NoAlarm;

        [DisplayName("倍率")]
        [JsonProperty]
        public double Coefficient
        {
            get => _coefficient;
            set
            {
                if (value == 0)
                {
                    return;
                }
                _coefficient = value;
                PointNum = 0;
                if (_coefficient < 1)  // 计算显示精度
                {
                    string strPoint = _coefficient.ToString();
                    PointNum = strPoint.Length - strPoint.IndexOf(".") - 1;
                }
            }
        }
        [DisplayName("小数位数")]
        [Browsable(false)]
        public int PointNum { get; set; } = 3;
        [DisplayName("检查阈值")]
        [JsonProperty]
        public bool IsCheckThreshold { get; set; }
        [DisplayName("最高阈值")]
        [JsonProperty]
        public double Toplimit { get; set; } = 285;
        [DisplayName("次高阈值")]
        [JsonProperty]
        public double SubToplimit { get; set; } = 245;
        [DisplayName("次低阈值")]
        [JsonProperty]
        public double SubLowerlimit { get; set; } = 200;
        [DisplayName("最低阈值")]
        [JsonProperty]
        public double Lowerlimit { get; set; } = 185;

    }

    // 遥测值报警类型
    public enum eAlarmType
    {
        NoAlarm = 0,
        LowLow = 1,
        Low = 2,
        Up = 3,
        UpUp = 4
    }
}
