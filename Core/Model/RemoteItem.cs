﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Core.Helper;

namespace Core.Model
{
    [DataContract]
    public class RemoteItem : INotifyPropertyChanged
    {
        private string _name;
        private string _channel;
        private string _device;
        private string _dataItem;
        private double _onValue;
        private double _offValue;
        private bool _hasAlarmWin;


        public RemoteItem()
        {
            Id = IdHelper.GuidToString();
        }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name
        {
            get => _name;
            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        [DataMember]
        public string Channel
        {
            get => _channel;
            set
            {
                if (_channel != value)
                {
                    _channel = value;
                    NotifyPropertyChanged("Channel");
                }
            }
        }

        [DataMember]
        public string Device
        {
            get => _device;
            set
            {
                if (_device != value)
                {
                    _device = value;
                    NotifyPropertyChanged("Device");
                }
               
            }
        }
        [DataMember]
        public string DataItem
        {
            get => _dataItem;
            set
            {
                if (_dataItem != value)
                {
                    _dataItem = value;
                    NotifyPropertyChanged("DataItem");
                }

            }
        }

        [DataMember]
        public string RelateItemId { get; set; } // 关联数据点Id
        [DataMember]
        public double OnValue
        {
            get => _onValue;
            set
            {
                if (Math.Abs(_onValue - value) > 0.0000001f)
                {
                    _onValue = value;
                    NotifyPropertyChanged("OnValue");
                }
               
            }
        }

        [DataMember]
        public double OffValue
        {
            get => _offValue;
            set
            {
                if (Math.Abs(_offValue - value) > 0.0000001f)
                {
                    _offValue = value;

                    NotifyPropertyChanged("OffValue");
                }
                
            }
        }

        public bool HasAlarmWin
        {
            get => _hasAlarmWin;
            set
            {
                if (_hasAlarmWin != value)
                {
                    _hasAlarmWin = value;
                    NotifyPropertyChanged("HasAlarmWin");
                }
            }
        }

        public void CloneTo(RemoteItem item)
        {
            if(item == null) return;
            item.Id = this.Id;
            item.Name = this.Name;
            item.Channel = this.Channel;
            item.Device = this.Device;
            item.DataItem = this.DataItem;
            item.RelateItemId = this.RelateItemId;
            item.OnValue = this.OnValue;
            item.OffValue = this.OffValue;
            item.HasAlarmWin = this.HasAlarmWin;

        }
        public void CloneFrom(RemoteItem item)
        {
            if (item == null) return;
             this.Id=item.Id;
            this.Name= item.Name ;
            this.Channel=item.Channel ;
            this.Device=item.Device;
            this.DataItem=item.DataItem;
            this.RelateItemId=item.RelateItemId;
            this.OnValue=item.OnValue;
            this.OffValue=item.OffValue;
            this.HasAlarmWin=item.HasAlarmWin;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
