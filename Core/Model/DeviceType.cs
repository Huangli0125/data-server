﻿using System.Runtime.Serialization;
using Core.Helper;

namespace Core.Model
{
    [DataContract]
    public class DeviceType
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }

        public DeviceType()
        {
            Id = IdHelper.GuidToString();
            Name = "新设备类型";
            Description = "设备类型描述信息";
        }
    }
}
