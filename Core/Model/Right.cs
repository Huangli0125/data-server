﻿using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class Right
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
