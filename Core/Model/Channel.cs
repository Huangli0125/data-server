﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Core.Helper;

namespace Core.Model
{
    /// <summary>
    /// 通道参数
    /// </summary>
    [DataContract]
    public class Channel:INotifyPropertyChanged
    {
        private int _readTimeout = 2000;
        private int _writeTimeout = 2000;
        private bool _connected = false;
        private bool _running = false;

        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public string Name { get; set; }
        //[DataMember]
        public string ChannelType { get; set; }
        //[DataMember]
        public int PollingDelay { get; set; }
        //[DataMember]
        public int ReadTimeout
        {
            get => _readTimeout;
            set
            {
                if (_readTimeout != value)
                {
                    _readTimeout = value;
                    OnPropertyChanged("ReadTimeout");
                }
            }
        }
        //[DataMember]
        public int WriteTimeout
        {
            get => _writeTimeout;
            set
            {
                if (_writeTimeout != value)
                {
                    _writeTimeout = value;
                    OnPropertyChanged("WriteTimeout");
                }
            }
        }
        //[DataMember]
        public string Ip { get; set; }
        //[DataMember]
        public int Port { get; set; }
        //[DataMember]
        public string Comport { get; set; }
        //[DataMember]
        public int Baudrate { get; set; }
        //[DataMember]
        public string Parity { get; set; }
        //[DataMember]
        public int DataBits { get; set; }
        //[DataMember]
        public string StopBits { get; set; }

        [DataMember]
        public bool IsConnected
        {
            get
            {
                bool temp =  Core.Global.NetAccess.IsChannelConnected(this.Id);
                if (temp != _connected)
                {
                    _connected = temp;
                    OnPropertyChanged("IsConnected");
                }

                return _connected;
            }
            set
            {
                
            }
        }
        [DataMember]
        public bool IsRunning
        {
            get
            {
                bool temp = Core.Global.NetAccess.IsChannelRunning(this.Id);
                if (temp != _running)
                {
                    _running = temp;
                    OnPropertyChanged("IsRunning");
                }

                return _running;
            }
            set
            {

            }
        }

        public Channel()
        {
            Id = IdHelper.GuidToString();
        }

        public Channel Clone(string newId, string newName)
        {
            Channel channel = new Channel();
            channel.Id = newId;
            channel.Name = newName;
            channel.Baudrate = Baudrate;
            channel.ChannelType = ChannelType;
            channel.Comport = Comport;
            channel.DataBits = DataBits;
            channel.StopBits = StopBits;
            channel.Parity = Parity;
            channel.Ip = Ip;
            channel.Port = Port;
            channel.PollingDelay = PollingDelay;
            channel.WriteTimeout = WriteTimeout;
            channel.ReadTimeout = ReadTimeout;
            return channel;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
