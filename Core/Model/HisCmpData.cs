﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class HisCmpData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DevId { get; set; }
        [DataMember]
        public string DevName { get; set; }
        [DataMember]
        public double Percent { get; set; }
       
    }
}
