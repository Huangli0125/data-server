﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class OperRecord
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public DateTime OperTime { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Operation { get; set; }

    }
}
