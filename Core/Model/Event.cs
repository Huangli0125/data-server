﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public DateTime EventTime { get; set; }
        [DataMember]
        public EventType EventType { get; set; }
        [DataMember]
        public EventLevel EventLevel { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsConfirm { get; set; }
        [DataMember]
        public DateTime ConfirmTime { get; set; }
        [DataMember]
        public string ConfirmMan { get; set; }
    }

    [DataContract]
    public enum EventType
    {
        [EnumMember]
        All=0,
        [EnumMember]
        System,
        [EnumMember]
        Communication,
        [EnumMember]
        ExceedLimit,
        [EnumMember]
        SignalChanged,
    }

    [DataContract]
    public enum EventLevel
    {
        [EnumMember]
        All=0,
        [EnumMember]
        Error,
        [EnumMember]
        Warning,
        [EnumMember]
        Information
    }
}
