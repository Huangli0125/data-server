﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class DeviceCategory : INotifyPropertyChanged
    {
        private string _name;
        private bool _isEditing;

        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if(value != _name)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }
        [DataMember]
        public bool IsEditing  // 客户端修改名称
        {
            get
            {
                return _isEditing;
            }
            set
            {
                if (value != _isEditing)
                {
                    _isEditing = value;
                    NotifyPropertyChanged("IsEditing");
                }
            }
        }
        [DataMember]
        public int Position { get; set; }

        [DataMember]
        public string Devices { get; set; }

        [DataMember]
        public Dictionary<string,Device> DeviceDict { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
