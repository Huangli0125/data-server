﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class MsgCtrl
    {
        public bool IsShow { get; set; } = false;
        public string SelectedChlId { get; set; } = "";
        public string SelectedDevId { get; set; } = "";
    }
}
