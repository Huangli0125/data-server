﻿using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class PageInfo
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsHomePage { get; set; }
        [DataMember]
        public byte[] Content { get; set; }
    }
}
