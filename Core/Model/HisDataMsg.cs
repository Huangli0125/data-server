﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class HisDataMsg
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DevId { get; set; }
        [DataMember]
        public string DevName { get; set; }
        [DataMember]
        public double RecValue { get; set; }
       
        public DateTime RecTime { get; set; }

        private string format = "";
        [DataMember]
        public string Time
        {
            get
            {
                if(!string.IsNullOrEmpty(format))
                    return RecTime.ToString(format);
                else 
                    return RecTime.ToString("yyyy-MM-dd HH:mm");
            }
            set => format = value;
        }
    }
}
