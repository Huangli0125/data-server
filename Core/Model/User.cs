﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class User
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        //[DataMember]
        public DateTime UpdateTime { get; set; }


    }
}
