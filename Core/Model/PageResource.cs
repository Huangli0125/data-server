﻿using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class PageResource
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FileExt { get; set; }
        [DataMember]
        public byte[] Content { get; set; }
    }
}
