﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class HisData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string DevId { get; set; }

        public bool IsSum { get; set; }
        [DataMember]
        public double RecValue { get; set; }
        [DataMember]
        public DateTime RecTime { get; set; }

        public HisData()

        {
            
        }

        public HisData(string dataitemId,string devId,bool issum,double val,DateTime recTime)
        {
            this.Id = dataitemId;
            this.DevId = devId;
            this.IsSum = issum;
            this.RecValue = val;
            this.RecTime = recTime;
        }
    }
}
