﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class RealTimeData
    {
        private Object _Value;
        private DateTime _UpdateTime = DateTime.Now;

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ParentName { get; set; }

        [DataMember]
        public Object Value
        {
            get => _Value;
            set
            {
                _Value = value;
                _UpdateTime = DateTime.Now;
            }
        }

        public DateTime UpdateTime
        {
            get => _UpdateTime;
            set => _UpdateTime = value;
        }
        [DataMember]
        public string Time
        {
            get
            {
                return UpdateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            set
            {
                ;
            }
        }

        public RealTimeData()
        {
            
        }

        public RealTimeData(string id, string name, object value =null, string parent ="")
        {
            this.Id = id;
            this.Name = name;
            this.Value = value;
            this.ParentName = parent;
        }
    }
}
