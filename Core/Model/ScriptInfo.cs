﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    [DataContract]
    public class ScriptInfo
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string PageId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public byte[] Content { get; set; }
    }
}
