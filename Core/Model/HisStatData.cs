﻿using System;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class HisStatData
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DevId { get; set; }
        [DataMember]
        public string DevName { get; set; }
        [DataMember]
        public double MaxValue { get; set; }
        [DataMember]
        public double MinValue { get; set; }
        [DataMember]
        public double AvgValue { get; set; }

    }
}
