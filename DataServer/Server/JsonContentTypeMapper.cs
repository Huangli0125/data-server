﻿using System.ServiceModel.Channels;

namespace DataServer.Server
{
    public class JsonContentTypeMapper : WebContentTypeMapper
    {
        public override WebContentFormat GetMessageFormatForContentType(string contentType)
        {
            if ("application/x-www-form-urlencoded" == contentType)
            {
                return WebContentFormat.Json;
            }
            return WebContentFormat.Default;
        }
    }

}
