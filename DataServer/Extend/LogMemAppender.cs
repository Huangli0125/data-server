﻿using log4net.Appender;
using log4net.Core;

namespace DataServer.Extend
{
    public class LogMemAppender: AppenderSkeleton
    {
        public delegate void ReceiveEvent(LoggingEvent evt);

        public event ReceiveEvent OnReceiveEvent ;

        private readonly object _synObj = new object();
 
         #region Override implementation of AppenderSkeleton
         protected override void Append(log4net.Core.LoggingEvent loggingEvent)
         {
             lock (_synObj)
             {
                 OnReceiveEvent?.Invoke(loggingEvent);
             }
         }
         #endregion

    }
}
