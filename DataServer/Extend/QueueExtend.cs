﻿using System.Collections.Generic;

namespace DataServer.Extend
{
    public static class QueueExtend
    {
        public static void Enqueue<T>(this Queue<T> queue, T item, int limit)
        {
            if (queue.Count >= limit)
            {
                queue.Dequeue();
            }
            queue.Enqueue(item);
        }
    }
}
