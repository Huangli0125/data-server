﻿using System;
using System.Collections.Generic;
using System.IO;
using Core;
using Core.Interface;
using Core.Model;
using Core.Model.Transmit;
using Core.Msg;
using DataServer.Config;
using DataServer.Server;
using EventHandler = Core.Interface.EventHandler;

namespace DataServer.Global
{
    public class DataAccess:IDataAccess
    {
        #region 协议驱动文件目录

        
        public string GetDevProtocolPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Resources.devprotocolpath);
        }

        public string GetChlProtocolPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Resources.chlprotocolpath);
        }
        public string GetTransmitDevProtocolPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Resources.transmitdevicepath);
        }

        public string GetTransmitChlProtocolPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Resources.transmitchannelpath);
        }
        #endregion

        #region 设备

        public List<Channel> GetAllChannels()
        {
            return AppManager.ChannelDao.GetEntities();
        }

        public Channel GetChannel(string id)
        {
            return AppManager.ChannelDao.GetEntityById(id);
        }

        public List<Device> GetDevices(string chlId)
        {
            return AppManager.DeviceDao.GetDevicesByChlId(chlId);
        }

        public List<DataItem> GetDataItems(string devId)
        {
            return AppManager.DataItemDao.GetDataItemsByDevId(devId);
        }

        public RemoteItem GetRemoteItem(string id)
        {
            return AppManager.RemoteItemDao.GetEntityById(id);
        }


        public OperateResult ModifyDataValue(string chlId, string devId, string dataId, double value, int timeout)
        {
            return AppManager.ModifyDataValue(chlId, devId, dataId, value, timeout);
        }

        public bool SaveDataItems(string devId, List<DataItem> dataItems)
        {
            AppManager.DataItemDao.DeleteDataItemsByDeviceId(devId);
            if (dataItems == null || dataItems.Count == 0)
                return true;
            return AppManager.DataItemDao.InsertEntities(dataItems);
        }

        public List<Operation> GetOperations(string devTypeId)
        {
            return AppManager.OperationDao.GetOperationByDevType(devTypeId);
        }

        #endregion

        #region 事件

        public bool AddEvent(EventType type, EventLevel level, string description)
        {
            Event evt = AppManager.AddEvent(type, level, description);
            if (evt == null) return false;
             PushService.PostEvent(new ArgumentBase<Event>(){Model = evt});
            return true;
        }

        #endregion

        #region 配置

        public T GetValueFromIni<T>(string section, string key, T defValue)
        {
            return ConfigManager.GetValueFromIni(section, key, defValue);
        }

        #endregion

        #region 转发数据

        public List<TransmitChannel> GetAllTransmitChannels()
        {
           return AppManager.TransmitChannelDao.GetEntities();
        }

        public TransmitChannel GetTransmitChannel(string id)
        {
            return AppManager.TransmitChannelDao.GetEntityById(id);
        }

        public List<TransmitDevice> GetTransmitDevices(string chlId)
        {
            return AppManager.TransmitDeviceDao.GetDevicesByChlId(chlId);
        }

        public List<TransmitData> GetTransmitData(string devId)
        {
            return AppManager.TransmitDataDao.GetDataItemsByDevId(devId);
        }

        public bool SaveTransmitDatas(string devId, List<TransmitData> dataItems)
        {
            AppManager.TransmitDataDao.DeleteDataItemsByDeviceId(devId);
            if (dataItems == null || dataItems.Count == 0)
                return true;
            return AppManager.TransmitDataDao.InsertEntities(dataItems);
        }

        /// <summary>
        /// 是否处理显示报文
        /// </summary>
        /// <param name="channelId">通道Id</param>
        /// <param name="deviceId">设备Id</param>
        /// <returns>true 需处理  false 不处理</returns>
        public bool ShowMessage(string channelId, string deviceId)
        {
            if (!AppManager.MsgCtrl.IsShow)
            {
                return false;
            }
            if(string.IsNullOrEmpty(AppManager.MsgCtrl.SelectedChlId) && string.IsNullOrEmpty(AppManager.MsgCtrl.SelectedDevId))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(AppManager.MsgCtrl.SelectedDevId))
            {
                return AppManager.MsgCtrl.SelectedDevId == deviceId;
            }
            else
            {
                return AppManager.MsgCtrl.SelectedChlId == channelId;
            }
        }

        #endregion

        #region 实时数据

        public event EventHandler.DataValueChanged OnDataValueChanged;

        public void PostData(string itemId, bool privilege, object value,Int32 triggerVal = 1)
        {
            AppManager.PostData(itemId, privilege, value);
            //if (privilege) // 信号量
            //{
                try
                {
                    OnDataValueChanged?.Invoke(itemId, value, triggerVal);
                }
                catch
                {
                }
            //}
           
        }

        public void ValueChanged(string itemId, object value, Int32 triggerVal = 1)
        {
            try
            {
                OnDataValueChanged?.Invoke(itemId, value, triggerVal);
            }
            catch
            {
            }
        }
        #endregion
    }
}
