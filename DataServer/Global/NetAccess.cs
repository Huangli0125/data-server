﻿using Core.Interface;

namespace DataServer.Global
{
    class NetAccess:INetAccess
    {
        public void SendWebsocketMsg(string key, object obj)
        {
            AppManager.SendWebsocketMsg(key,obj);
        }

        public bool IsChannelConnected(string id)
        {
           var  chl = AppManager.Channels.Find(sss => sss.ChlInfo.Id == id);
            if (chl != null)
            {
                return chl.Connected;
            }
            return false;
        }

        public bool IsChannelRunning(string id)
        {
            var chl = AppManager.Channels.Find(sss => sss.ChlInfo.Id == id);
            if (chl != null)
            {
                return chl.Running;
            }
            return false;
        }
    }
}
