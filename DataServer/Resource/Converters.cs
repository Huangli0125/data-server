﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DataServer.Resource
{
    [ValueConversion(typeof(bool), typeof(BitmapImage))]
    public class ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (value.Equals(true))
            {
                return new BitmapImage(new Uri("Image/ok.png", UriKind.Relative));
            }
            else
            {
                return new BitmapImage(new Uri("Image/error.png", UriKind.Relative));
            }
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(int), typeof(string))]
    public class ComTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (value.Equals(1))
            {
                return "RS485";
            }
            else
            {
                return "RS232"; 
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (value.Equals("RS485"))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
