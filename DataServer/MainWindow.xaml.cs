﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using DataServer.View;
using log4net;
using ContextMenu = System.Windows.Forms.ContextMenu;
using MenuItem = System.Windows.Forms.MenuItem;

namespace DataServer
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MainWindow));
        private NotifyIcon _NotifyIcon;
        private bool _IsShutdown;

        public MainWindow()
        {
            
            InitializeComponent();
            Loaded += MainWindow_Loaded;
         
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            AppManager.InitMainWindow(this);
            LoadNotifyIcon();
            WaitingBox.Show(this, () =>
            {
                AppManager.OnCommunicationRestart += AppManagerOnCommunicationRestart;
                AppManager.Initialize();
                AppManager.StartCommunication(false);
                AppManager.StartWcfService();
 
            }, "系统任务启动中，请稍后...");
            AppManager.TransmitInitialize(); // 转发
            AppManager.StartTransmit(false);
            //Task.Factory.StartNew(() =>
            //{
            //    AppManager.OnCommunicationRestart += AppManagerOnCommunicationRestart;
            //    AppManager.Initialize();
            //    AppManager.StartCommunication(false);
            //    AppManager.StartWcfService();
            //});
        }

       
        private void AppManagerOnCommunicationRestart(object sender, EventArgs e)
        {
           // msgPanel.AddMsgDealEvent(); // 在View 中处理
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (!_IsShutdown)
            {
                e.Cancel = true;
                this.ShowInTaskbar = false;
                this.Hide();
                return;
            }
            _NotifyIcon.Visible = false;
            base.OnClosing(e);
        }


        /// <summary>
        /// 托盘处理
        /// </summary>
        private void LoadNotifyIcon()
        {
            this.ShowInTaskbar = true;
            this._NotifyIcon = new NotifyIcon();
            this._NotifyIcon.BalloonTipText = this.Title;
            this._NotifyIcon.ShowBalloonTip(2000);
            this._NotifyIcon.Text = this.Title;
            this._NotifyIcon.Icon = System.Drawing.Icon.ExtractAssociatedIcon(System.Windows.Forms.Application.ExecutablePath);
            this._NotifyIcon.Visible = true;

            //退出菜单项
            bool confirming = false;
            MenuItem exit = new MenuItem("退出");
            exit.Click += (sender, e) =>
            {
                LoginView loginView = new LoginView();
                if (loginView.ShowDialog() != true)
                {
                    return;
                }
                if (confirming)
                {
                    return;
                }
                confirming = true;
                MessageBoxResult mbr = System.Windows.MessageBox.Show("您确定要关闭“" + this.Title + "”吗？", this.Title, MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel);
                if (mbr == MessageBoxResult.OK)
                {
                    _IsShutdown = true;
                    _log.Warn($"系统被退出,用户为{loginView.LoginUser?.Name}");
                    System.Windows.Application.Current.Shutdown(0);
                }
                confirming = false;
            };
            MenuItem register = new MenuItem("注册");
            register.Click += (sender, e) =>
            {
                RegisterView view = new RegisterView();
                view.ShowDialog();
            };

            //关联托盘控件
            MenuItem[] childen = new MenuItem[] { register,exit };
            _NotifyIcon.ContextMenu = new ContextMenu(childen);

            this._NotifyIcon.MouseDoubleClick += (o, e) =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    LoginView loginView = new LoginView();
                    if (loginView.ShowDialog() == true)
                    {
                        this.Visibility = Visibility.Visible;
                        this.ShowInTaskbar = true;
                        this.Activate();
                    }
                }
            };

        }

        #region 最大化最小化关闭按钮事件
        private void DazzleButtonMax_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }

        }
        private void DazzleButtonMin_Click(object sender, RoutedEventArgs e)
        {
            //this.ShowInTaskbar = false;
            this.WindowState = WindowState.Minimized;
        }
        private void DazzleButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion
    }
}
