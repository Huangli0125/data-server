﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Core.Model;
using DataServer.Config;
using log4net;

namespace DataServer.View
{
    /// <summary>
    /// DeviceTypeView.xaml 的交互逻辑
    /// </summary>
    public partial class DeviceTypeView 
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ObservableCollection<DeviceType> _deviceTypes = new ObservableCollection<DeviceType>();
        private readonly ObservableCollection<Operation> _operations = new ObservableCollection<Operation>();

        private DeviceType _selectDeviceType;
        private Operation _selectOperation;
        public DeviceTypeView()
        {
            InitializeComponent();
            Loaded += DeviceTypeView_Loaded;
        }

        private void DeviceTypeView_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<DeviceType> deviceTypes = AppManager.DeviceTypeDao.GetEntities();
                if (deviceTypes != null && deviceTypes.Count > 0)
                {
                    foreach (DeviceType deviceType in deviceTypes)
                    {
                        _deviceTypes.Add(deviceType);
                    }
                }
                listviewDeviceType.ItemsSource = _deviceTypes;
                listviewOpertion.ItemsSource = _operations;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
        }
        private void ListviewDeviceType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _operations.Clear();
                _selectDeviceType = listviewDeviceType.SelectedItem as  DeviceType;
                if (_selectDeviceType != null)
                {
                    List<Operation> operations = AppManager.OperationDao.GetOperationByDevType(_selectDeviceType.Id);
                    if (operations != null && operations.Count > 0)
                    {
                        foreach (Operation operation in operations)
                        {
                            _operations.Add(operation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
        }

        private void ListviewOpertion_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectOperation = listviewOpertion.SelectedItem as Operation;
        }

        private void BtnAddDeviceType_OnClick(object sender, RoutedEventArgs e)
        {
            _deviceTypes.Add(new DeviceType());
            listviewDeviceType.SelectedItem = _deviceTypes[_deviceTypes.Count - 1];
        }

        private void BtnDelDeviceType_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectDeviceType == null)
            {
                MessageBox.Show("请选择设备类型!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            _deviceTypes.Remove(_selectDeviceType);
        }

        private void BtnSaveDeviceType_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                AppManager.DeviceTypeDao.DeleteAllEntities();
                if (_deviceTypes.Count > 0)
                {
                    if (AppManager.DeviceTypeDao.InsertEntities(_deviceTypes.ToList()))
                    {
                        MessageBox.Show("保存成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("保存失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    return;
                }
                MessageBox.Show("保存成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
               _log.Info(ex.Message);
            }
        }

        private void BtnAddOperation_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectDeviceType == null)
            {
                MessageBox.Show("请选中设备类型!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            _operations.Add(new Operation(){DevTypeId = _selectDeviceType .Id,Timeout = 1000});
            listviewDeviceType.SelectedItem = _operations[_operations.Count - 1];
        }

        private void BtnDelOperation_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectOperation == null)
            {
                MessageBox.Show("请选择设备类型!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            _operations.Remove(_selectOperation);
        }

        private void BtnSaveOperation_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_selectDeviceType == null)
                {
                    MessageBox.Show("当前没有选中设备类型,不能保存!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                AppManager.OperationDao.DeleteOperationByDevType(_selectDeviceType.Id);
                if (_operations.Count > 0)
                {
                    if (AppManager.OperationDao.InsertEntities(_operations.ToList()))
                    {
                        MessageBox.Show("保存成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("保存失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    return;
                }
                MessageBox.Show("保存成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                _log.Info(ex.Message);
            }
        }
        private void BtnExportOperation_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectDeviceType == null)
            {
                MessageBox.Show("请先选择设备类型!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (_operations.Count == 0)
            {
                MessageBox.Show("没有可导出的信息!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (ConfigManager.ExportOperations(_selectDeviceType.Name, _operations.ToList()))
            {
                MessageBox.Show("导出成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("导出失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BtnImportOperation_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectDeviceType == null)
            {
                MessageBox.Show("请先创建或选择设备类型!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            List<Operation> operations = ConfigManager.ImportOperations();
            if (operations == null || operations.Count == 0)
            {
                MessageBox.Show("导入失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            _operations.Clear();
            foreach (var sss in operations)
            {
                sss.DevTypeId = _selectDeviceType.Id;
                _operations.Add(sss);
            }
            MessageBox.Show("导入成功!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
