﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Core.Model;

namespace DataServer.View
{
    /// <summary>
    /// RemoteItemMdyWin.xaml 的交互逻辑
    /// </summary>
    public partial class RemoteItemMdyWin
    {
        public RemoteItem RemoteControlItem { get; private set; }
        private readonly ObservableCollection<Channel> _channels = new ObservableCollection<Channel>();
        private readonly ObservableCollection<Device> _devices = new ObservableCollection<Device>();
        private readonly ObservableCollection<DataItem> _dataItems = new ObservableCollection<DataItem>();
        private Channel _selectChannel;
        private Device _selectDevice;
        private DataItem _selectDataItem;
        public RemoteItemMdyWin(RemoteItem item)
        {
            InitializeComponent();
            RemoteControlItem = item;
            rightButton.MiniWindow += RightButton_MiniWindow;
            rightButton.MaxiWindow += RightButton_MaxiWindow;
            rightButton.CloseWindow += RightButton_CloseWindow;
            this.Loaded += RemoteItemMdyWin_Loaded;
        }

        private void RemoteItemMdyWin_Loaded(object sender, RoutedEventArgs e)
        {
            _channels.Clear();
            List<Channel> channels = AppManager.ChannelDao.GetEntities();
            if (channels != null)
            {
                foreach (Channel ch in channels)
                {
                    _channels.Add(ch);
                }
            }
            cbbChannel.ItemsSource = _channels;
            cbbAlarmWin.Items.Add("无");
            cbbAlarmWin.Items.Add("生效中");
            this.DataContext = RemoteControlItem;
            UpdateSelectedItem();
        }
        private void UpdateSelectedItem()
        {
            try
            {
               
                if (RemoteControlItem == null) return;
                DataItem dataItem = AppManager.DataItemDao.GetEntityById(RemoteControlItem.RelateItemId);
                if (dataItem != null)
                {
                    Device device = AppManager.DeviceDao.GetEntityById(dataItem.DeviceId);
                    if (device != null)
                    {
                        Channel channel = _channels.FirstOrDefault(sss => sss.Id == device.ChannelId);
                        if (channel != null)
                        {
                            cbbChannel.SelectedItem = channel;
                            _devices.Clear();
                            List<Device> devices = AppManager.DeviceDao.GetDevicesByChlId(channel.Id);
                            if (devices != null)
                            {
                                foreach (Device dev in devices)
                                {
                                    _devices.Add(dev);
                                    if (dev.Id == device.Id)
                                        device = dev;
                                }
                            }
                            cbbDevice.ItemsSource = _devices;
                            cbbDevice.SelectedItem = device;
                            _dataItems.Clear();
                            List<DataItem> dataItems = AppManager.DataItemDao.GetDataItemsByDevId(device.Id);
                            if (dataItems != null)
                            {
                                foreach (DataItem item in dataItems)
                                {
                                    _dataItems.Add(item);
                                    if (item.Id == dataItem.Id)
                                        dataItem = item;
                                }
                            }
                            cbbDataItem.ItemsSource = _dataItems;
                            cbbDataItem.SelectedItem = dataItem;
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void CbbChannel_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(cbbChannel.SelectedItem is Channel))
            {
                if (_selectChannel != null)
                    cbbChannel.SelectedItem = _selectChannel;
                return;
            }
            _selectDevice = null;
            _selectChannel = cbbChannel.SelectedItem as Channel;
            _devices.Clear();
            if (_selectChannel != null)
            {
                List<Device> devices = AppManager.DeviceDao.GetDevicesByChlId(_selectChannel.Id);
                if (devices != null)
                {
                    foreach (Device dev in devices)
                    {
                        _devices.Add(dev);
                    }
                }

            }
            cbbDevice.ItemsSource = _devices;
        }

        private void CbbDevice_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectDataItem = null;
            _selectDevice = cbbDevice.SelectedItem as Device;
            _dataItems.Clear();
            if (_selectDevice != null)
            {
                List<DataItem> dataItems = AppManager.DataItemDao.GetDataItemsByDevId(_selectDevice.Id);
                if (dataItems != null)
                {
                    foreach (DataItem item in dataItems)
                    {
                        if(item.Access != Access.只读)
                            _dataItems.Add(item);
                    }
                }

            }
            cbbDataItem.ItemsSource = _dataItems;
        }

        private void CbbDataItem_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectDataItem = cbbDataItem.SelectedItem as DataItem;
            if (_selectDataItem != null) RemoteControlItem.RelateItemId = _selectDataItem.Id;
        }

        private void Btn_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        #region 窗体最大化最小化关闭事件处理

        private void RightButton_CloseWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            this.Close();
        }

        private void RightButton_MaxiWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        private void RightButton_MiniWindow(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        #endregion
    }
}
