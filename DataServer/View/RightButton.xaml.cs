﻿using System.Windows;

namespace DataServer.View
{
    /// <summary>
    /// RightButton.xaml 的交互逻辑
    /// </summary>
    public partial class RightButton
    {
        public event RoutedEventHandler MiniWindow;
        public event RoutedEventHandler MaxiWindow;
        public event RoutedEventHandler CloseWindow;

        /// <summary>
        /// 是否显示最大化按钮
        /// </summary>
        public bool IsShowMax
        {
            get => (bool)GetValue(IsShowMaxProperty);
            set => SetValue(IsShowMaxProperty, value);
        }
        //定义依赖属性
        public static readonly DependencyProperty IsShowMaxProperty = DependencyProperty.Register("IsShowMax", typeof(bool), typeof(RightButton),
            new UIPropertyMetadata(true, (o, args) => {
                if (o is RightButton)
                {
                    var sender = o as RightButton;
                    if(sender.IsShowMax)
                        sender.btnMax.Visibility = Visibility.Visible;
                    else
                        sender.btnMax.Visibility = Visibility.Collapsed;
                    
                }
            }));

        public RightButton()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MaxiWindow?.Invoke(this, e);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MiniWindow?.Invoke(this, e);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CloseWindow?.Invoke(this, e);
        }

        
    }
}
