﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Core.Model;
using log4net;
using Channel = Core.Model.Channel;

namespace DataServer.View
{
    /// <summary>
    /// DeviceView.xaml 的交互逻辑
    /// </summary>
    public partial class HisReportView
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ObservableCollection<Channel> _channels = new ObservableCollection<Channel>();
        private readonly ObservableCollection<Device> _devices = new ObservableCollection<Device>();
        private Channel selectChannel;
        private Device selectDevice;
        private readonly ObservableCollection<DataItem> _dataItems = new ObservableCollection<DataItem>();

        public HisReportView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            this.Loaded += RealTimeView_Loaded; ;
        }

        private void RealTimeView_Loaded(object sender, RoutedEventArgs e)
        {
            _channels.Clear();
            List<Channel> channels = AppManager.ChannelDao.GetEntities();
            if (channels != null)
            {
                foreach (Channel ch in channels)
                {
                    _channels.Add(ch);
                }
            }
            channelListBox.DataContext = _channels;
            if (_channels.Count > 0)
            {
                channelListBox.SelectedItem = _channels[0];
            }
            btnStartComm.IsEnabled = !AppManager.IsRunning;
            btnStopComm.IsEnabled = AppManager.IsRunning;
        }

        private void ChannelListBox_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            selectDevice = null;
        }
        private void ChannelListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectDevice = null;
            selectChannel = channelListBox.SelectedItem as Channel;
           
            _devices.Clear();
            if (selectChannel != null)
            {
                List<Device> devices = AppManager.DeviceDao.GetDevicesByChlId(selectChannel.Id);
                if (devices != null)
                {
                    foreach (Device dev in devices)
                    {
                        dev.DeviceType = AppManager.DeviceTypeDao.GetEntityById(dev.DeviceTypeId);
                        _devices.Add(dev);
                    }
                }
                deviceListBox.DataContext = _devices;
                if(_devices.Count>0)
                    deviceListBox.SelectedItem = _devices[0];
            }
            
        }
        private void DeviceListBox_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            selectDevice = deviceListBox.SelectedItem as Device;
            UpdateDataItems();
        }
        private void DeviceListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectDevice = deviceListBox.SelectedItem as Device;
            UpdateDataItems();
        }
        private void BtnStartComm_OnClick(object sender, RoutedEventArgs e)
        {
            AppManager.StartCommunication();
            btnStartComm.IsEnabled = false;
            btnStopComm.IsEnabled = true;
        }
        private void BtnStopComm_OnClick(object sender, RoutedEventArgs e)
        {
            AppManager.StopCommunication();
            btnStartComm.IsEnabled = true;
            btnStopComm.IsEnabled = false;
        }

        public void UpdateDataItems()
        {
            if (selectDevice == null)
            {
                _dataItems.Clear();
                return;
            }
            List<DataItem> dataItems;
            if (AppManager.IsRunning)
            {
                dataItems = AppManager.GetDataItemsByDevice(selectDevice);
                _dataItems.Clear();
                if (dataItems.Count > 0)
                {
                    foreach (DataItem item in dataItems)
                    {
                        _dataItems.Add(item);
                    }
                }

                _gridDataItem.ItemsSource = _dataItems;
                return;
            }

            
            _dataItems.Clear();
            dataItems = Core.Global.DataAccess.GetDataItems(selectDevice.Id);
            if (dataItems.Count > 0)
            {
                foreach (DataItem item in dataItems)
                {
                    _dataItems.Add(item);
                }
            }
            _gridDataItem.ItemsSource = _dataItems;

        }

       
    }
}
