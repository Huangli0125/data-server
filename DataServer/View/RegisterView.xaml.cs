﻿using System.Windows;
using System.Windows.Media;
using Core.Helper;

namespace DataServer.View
{
    /// <summary>
    /// RegisterView.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterView
    {
        public RegisterView()
        {
            InitializeComponent();
            rightButton.MiniWindow += RightButton_MiniWindow;
            rightButton.MaxiWindow += RightButton_MaxiWindow;
            rightButton.CloseWindow += RightButton_CloseWindow;
            this.Loaded += RegisterView_Loaded;
        }

        private void RegisterView_Loaded(object sender, RoutedEventArgs e)
        {
            txtLocalCode.Text = (Computer.CurrentIns.AuthSerialID + RegisterHelper.ExtensionStr).GetHashCode().ToString();
            RegisterHelper.CheckRegisteState();
            UpdateDisp();
        }

        private void BtnRegister_OnClick(object sender, RoutedEventArgs e)
        {
            if (btnRegister.Content.Equals("注    册"))
            {
                if (!string.IsNullOrEmpty(txtRegisterCode.Text) && !string.IsNullOrEmpty(txtAdditionCode.Text))
                {
                    RegisterHelper.RegisteSolfware(txtRegisterCode.Text, txtAdditionCode.Text);
                }
                else
                {
                    MessageBox.Show("注册码或附加码不能为空!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                UpdateDisp();
            }
            else
            {
                EnableDisp();
            }
        }

        private void UpdateDisp()
        {
            txtStatus.Foreground = RegisterHelper.IsRegisted?Brushes.Green:Brushes.Red;
            txtStatus.Text = RegisterHelper.IsRegisted ? "产品已注册" : "产品未注册";
            if (RegisterHelper.IsExpire)
            {
                txtStatus.Foreground =  Brushes.Red;
                txtStatus.Text = "产品已过期";
            }
            txtLocalCode.IsEnabled = !RegisterHelper.IsRegisted;
            txtRegisterCode.IsEnabled = !RegisterHelper.IsRegisted;
            txtAdditionCode.IsEnabled = !RegisterHelper.IsRegisted;
            txtAdditionCode.Text = RegisterHelper.ExpireRegCode;
  
            if (RegisterHelper.IsRegisted)
            {
                txtRegisterCode.Text = RegisterHelper.RegSerial;
                btnRegister.Content = "重新注册";
            }

        }
        private void EnableDisp()
        {
            txtLocalCode.IsEnabled = true;
            txtRegisterCode.IsEnabled = true;
            txtAdditionCode.IsEnabled = true;
            btnRegister.Content = "注    册";
        }
        #region 窗体最大化最小化关闭事件处理

        private void RightButton_CloseWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            this.Close();
        }

        private void RightButton_MaxiWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        private void RightButton_MiniWindow(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        #endregion
    }
}
