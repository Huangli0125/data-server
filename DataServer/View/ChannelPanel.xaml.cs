﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Core.ChannelProtocol;
using Channel = Core.Model.Channel;

namespace DataServer.View
{
    /// <summary>
    /// ChannelPanel.xaml 的交互逻辑
    /// </summary>
    public partial class ChannelPanel
    {
        private readonly Dictionary<string, FileVersionInfo> _protocolFiles = new Dictionary<string, FileVersionInfo>();
        private Channel _curChannel = null;
        public ChannelPanel()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            this.DataContextChanged += ChannelPanel_DataContextChanged;
            string strPath = Properties.Resources.chlprotocolpath;
            DirectoryInfo driverPath = new DirectoryInfo(strPath);
            FileInfo[] files = driverPath.GetFiles();
            _protocolFiles.Clear();
            for (int i = 0; i < files.Length; i++)
            {
                FileVersionInfo info = FileVersionInfo.GetVersionInfo(files[i].FullName);
                if (info.ProductName!=null && info.ProductName.Contains("Channel"))
                {
                    cbbChannelType.Items.Add(info.ProductName);
                    _protocolFiles.Add(info.ProductName,info);
                }
            }

            for (int i = 1; i < 51; i++)
            {
                cbbComport.Items.Add("COM"+i);
            }
            cbbBaudrate.Items.Add(2400);
            cbbBaudrate.Items.Add(4800);
            cbbBaudrate.Items.Add(9600);
            cbbBaudrate.Items.Add(19200);
            cbbBaudrate.Items.Add(38400);
            cbbBaudrate.Items.Add(57600);
            cbbBaudrate.Items.Add(115200);
            cbbParity.Items.Add("None");
            cbbParity.Items.Add("Even");
            cbbParity.Items.Add("Odd");
            cbbDataBits.Items.Add(6);
            cbbDataBits.Items.Add(7);
            cbbDataBits.Items.Add(8);
            cbbStopBits.Items.Add("None");
            cbbStopBits.Items.Add("One");
            cbbStopBits.Items.Add("Two");

        }

        private void ChannelPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
           _curChannel= this.DataContext as Channel;
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbbChannelType.SelectedItem != null)
            {
                string select = cbbChannelType.SelectedItem.ToString();
                if (_protocolFiles.ContainsKey(select))
                {
                    if (_protocolFiles[select].Comments.Contains("Net"))
                    {
                        gpSerialPort.Visibility = Visibility.Collapsed;
                        gpNet.Visibility = Visibility.Visible;
                        if (_curChannel != null)
                        {
                            if (_curChannel.ReadTimeout == 25)
                            {
                                _curChannel.ReadTimeout = 2000;
                            }
                            if (_curChannel.WriteTimeout == 25)
                            {
                                _curChannel.WriteTimeout = 2000;
                            }
                        }
                        return;
                    }
                    else if (_protocolFiles[select].Comments.Contains("SerialPort"))
                    {
                        gpNet.Visibility = Visibility.Collapsed;
                        gpSerialPort.Visibility = Visibility.Visible;
                        if (_curChannel != null)
                        {
                            if (_curChannel.ReadTimeout == 2000)
                            {
                                _curChannel.ReadTimeout = 25;
                            }
                            if (_curChannel.WriteTimeout == 2000)
                            {
                                _curChannel.WriteTimeout = 25;
                            }
                        }
                        return;
                    }
                }
            }
            gpSerialPort.Visibility = Visibility.Collapsed;
            gpNet.Visibility = Visibility.Visible;
        }
    }
}
