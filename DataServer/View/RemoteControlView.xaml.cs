﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Core.ChannelProtocol;
using Core.Model;
using Core.Msg;

namespace DataServer.View
{
    /// <summary>
    /// RemoteControlView.xaml 的交互逻辑
    /// </summary>
    public partial class RemoteControlView
    {
        private readonly ObservableCollection<RemoteItem> _remoteItems = new ObservableCollection<RemoteItem>();

        private RemoteItem _selectedItem;
        
        public RemoteControlView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }
            this.DataContext = _remoteItems;
            this.Loaded += RemoteControl_Loaded;
        }

        private void RemoteControl_Loaded(object sender, RoutedEventArgs e)
        {
            _remoteItems.Clear();
            List<RemoteItem> remoteItems = AppManager.RemoteItemDao.GetEntities();
            if (remoteItems != null)
            {
                foreach (RemoteItem item in remoteItems)
                {
                    _remoteItems.Add(item);
                }
            }
        }

        private void Add_MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            RemoteItem item = new RemoteItem();
            RemoteItemMdyWin win = new RemoteItemMdyWin(item);
            if (win.ShowDialog() == true)
            {
                item = win.RemoteControlItem;
                _remoteItems.Add(item);
                if (!AppManager.RemoteItemDao.InsertEntity(item))
                {
                    MessageBox.Show("数据库值添加失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Modify_MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectedItem == null)
            {
                MessageBox.Show("请选择编辑的遥控点!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            RemoteItem item = new RemoteItem();
            _selectedItem.CloneTo(item);
            RemoteItemMdyWin win = new RemoteItemMdyWin(item);
            if (win.ShowDialog() == true)
            {
                _selectedItem.CloneFrom(item);
                if (!AppManager.RemoteItemDao.UpdateEntity(_selectedItem))
                {
                    MessageBox.Show("数据库值更新失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Delete_MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectedItem == null)
            {
                MessageBox.Show("请选择删除的遥控点!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (MessageBox.Show("是否要删除遥控点!", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                MessageBoxResult.Yes)
            {
                if (!AppManager.RemoteItemDao.DeleteItemById(_selectedItem.Id))
                {
                    MessageBox.Show("数据库值删除失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                _remoteItems.Remove(_selectedItem);
            }
           
        }

        private void ListViewRemoteItem_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectedItem = listViewRemoteItem.SelectedItem as RemoteItem;
        }

        private void ListViewRemoteItem_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _selectedItem = listViewRemoteItem.SelectedItem as RemoteItem;
            Modify_MenuItem_OnClick(null, null);
        }

        private void Reload_MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            _remoteItems.Clear();
            List<RemoteItem> remoteItems = AppManager.RemoteItemDao.GetEntities();
            if (remoteItems != null)
            {
                foreach (RemoteItem item in remoteItems)
                {
                    _remoteItems.Add(item);
                }
            }
        }
    }

    [ValueConversion(typeof(bool), typeof(string))]
    public class AlarmWinConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool val)) return null;

            if (val) return "生效中";
            else return "无";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string val)) return false;
            if (val == "生效中") return true;
            else return false;
        }
    }

    public class NameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 3) return "";
            return values[0] + ">" + values[1] + ">" + values[2];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            string[] splitValues = ((string)value).Split('>');
            return splitValues;
        }
    }
  
}
