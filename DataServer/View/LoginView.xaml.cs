﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Core.Helper;
using Core.Model;
using Core.Msg;

namespace DataServer.View
{
    /// <summary>
    /// LoginView.xaml 的交互逻辑
    /// </summary>
    public partial class LoginView
    {
        public User LoginUser;
        private readonly ObservableCollection<User> _users = new ObservableCollection<User>();
       
        #region 构造函数

        public LoginView()
        {
            InitializeComponent();
            this.Loaded += LoginView_Loaded;
            this.Unloaded += LoginView_Unloaded;
            cbbUser.DataContext = _users;
            rightButton.MiniWindow += RightButton_MiniWindow;
            rightButton.MaxiWindow += RightButton_MaxiWindow;
            rightButton.CloseWindow += RightButton_CloseWindow;
            
        }

        #endregion

        #region 窗体事件

        private void LoginView_Loaded(object sender, RoutedEventArgs e)
        {
            tipPopup.PlacementTarget = btnLogin;
            _users.Clear();
            var users = AppManager.UserDao.GetEntities();
            if (users != null && users.Count > 0)
            {
                foreach (var item in users)
                {
                    _users.Add(item);
                }
            }
            else
            {
                tipPopup.IsOpen = true;
                tipPopup.PlacementTarget = btnLogin;
                txtTipContent.Text = "请检查数据库是否正常运行!";
            }
        }
        private void LoginView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_users == null || _users.Count == 0)
            {
                this.DialogResult = true;
                this.Close();
                return;
            }
            _users.Clear();
        }
       
        protected override void OnMouseMove(MouseEventArgs e)
        {
            Dispatcher?.BeginInvoke(new Action(() => {
                if (e.LeftButton == MouseButtonState.Pressed && !passwordBox.IsMouseCaptured)
                {
                    this.DragMove();
                }
                base.OnMouseMove(e);
            }));
        }

        #endregion
     

        #region 登录信息处理

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            tipPopup.IsOpen = false;
            User user = cbbUser.SelectedItem as User;
            if (user == null)
            {
                tipPopup.IsOpen = true;
                tipPopup.PlacementTarget = btnLogin;
                txtTipContent.Text = "请选择登录用户!";
                return;
            }
            if (string.IsNullOrEmpty(passwordBox.Password))
            {
                tipPopup.IsOpen = true;
                tipPopup.PlacementTarget = btnLogin;
                txtTipContent.Text = "密码不能为空!";
                return;
            }
            string name = user.Name;
            string password = passwordBox.Password;
            password = EncryptHelper.EncryptDES(password, "user_password");
            var result = AppManager.Login(name, password);
            if (result.Result == eResult.S_OK)
            {
                
                this.LoginUser = user;
                 List<Right> rights= AppManager.RightDao.GetUserRights(user.Id);
                foreach (Right r in rights)
                {
                    if (r.Name == "修改数据")
                    {
                        this.DialogResult = true;
                        this.Close();
                        return;
                    }
                }
                tipPopup.IsOpen = true;
                tipPopup.PlacementTarget = btnLogin;
                txtTipContent.Text = "该用户没有权限操作服务端程序!";
            }
            else
            {
                tipPopup.IsOpen = true;
                tipPopup.PlacementTarget = btnLogin;
                txtTipContent.Text = "登录失败!";
            }
        }

        private void PasswordBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnLogin_Click(null, null);
            }
        }

        
        #endregion

        #region 窗体最大化最小化关闭事件处理

        private void RightButton_CloseWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            this.Close();
        }

        private void RightButton_MaxiWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        private void RightButton_MiniWindow(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        #endregion

    }

}
