﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataServer.View.Transmit
{
    public class TransmitDataModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSignal { get; set; }
        public int Radio { get; set; } = 1;
    }
}
