﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using log4net;

namespace DataServer.View.Transmit
{
    /// <summary>
    /// DevicePanel.xaml 的交互逻辑
    /// </summary>
    public partial class TransmitDevicePanel
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private bool _isInit;
        public TransmitDevicePanel()
        {
            InitializeComponent();
            this.Loaded += DevicePanel_Loaded;
        }

        private void DevicePanel_Loaded(object sender, RoutedEventArgs e)
        {
            if(_isInit)
                return;
            try
            {
                if (cbbProtocol.Items.Count == 0)
                {
                    string strPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory +
                                                  Properties.Resources.transmitdevicepath);
                    DirectoryInfo driverPath = new DirectoryInfo(strPath);
                    FileInfo[] files = driverPath.GetFiles();
                    cbbProtocol.Items.Clear();
                    for (int i = 0; i < files.Length; i++)
                    {
                        FileVersionInfo info = FileVersionInfo.GetVersionInfo(files[i].FullName);
                        if (info.ProductName != null && info.ProductName.Contains("Protocol"))
                        {
                            cbbProtocol.Items.Add(info.ProductName);
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
            _isInit = true;

        }

    }

   
}
