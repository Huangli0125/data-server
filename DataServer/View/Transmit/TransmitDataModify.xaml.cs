﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using Core.DeviceProtocol;
using Core.Modbus;
using Core.Model;
using Core.Model.Transmit;

namespace DataServer.View.Transmit
{
    /// <summary>
    /// TransmitDataModify.xaml 的交互逻辑
    /// </summary>
    public partial class TransmitDataModify
    {
        private readonly ObservableCollection<Channel> _channels = new ObservableCollection<Channel>();
        private readonly ObservableCollection<Device> _devices = new ObservableCollection<Device>();
        private readonly ObservableCollection<TransmitDataModel> _selectedItems = new ObservableCollection<TransmitDataModel>();
        private readonly ObservableCollection<TransmitDataModel> _unSelectedItems = new ObservableCollection<TransmitDataModel>();
        public ObservableCollection<TransmitDataModel> SelectedItems => _selectedItems;
        public List<TransmitDataModel> NewSelected { get; set; } = new List<TransmitDataModel>();
        private Channel _selectChannel;
        private Device _selectDevice;
        public TransmitDataModify(List<TransmitData> transDatas)
        {
            InitializeComponent();
            _selectedItems.Clear();
            _unSelectedItems.Clear();
            try
            {
                foreach (var sss in transDatas)
                {
                    _selectedItems.Add(new TransmitDataModel()
                    { Id = sss.SourceId, Name = sss.Name, Radio = (int)(1 / sss.Coefficient), IsSignal = sss.PhyType == PhyType.信号量 });
                }
            }
            catch
            {
                MessageBox.Show("已选数据倍率不能为0!", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
            _selectedList.ItemsSource = _selectedItems;
            _unSelectedList.ItemsSource = _unSelectedItems;
            rightButton.MiniWindow += RightButton_MiniWindow;
            rightButton.MaxiWindow += RightButton_MaxiWindow;
            rightButton.CloseWindow += RightButton_CloseWindow;
            
            this.Loaded += TransmitDataModify_Loaded;
          
        }

        private void TransmitDataModify_Loaded(object sender, RoutedEventArgs e)
        {
            _channels.Clear();
            List<Channel> channels = AppManager.ChannelDao.GetEntities();
            if (channels != null)
            {
                foreach (Channel ch in channels)
                {
                    _channels.Add(ch);
                }
            }
            cbbChannel.ItemsSource = _channels;
            if (_channels.Count > 0)
                cbbChannel.SelectedItem = _channels[0];
        }

       

        private void CbbChannel_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectDevice = null;
            _selectChannel = cbbChannel.SelectedItem as Channel;
            _devices.Clear();
            if (_selectChannel != null)
            {
                List<Device> devices = AppManager.DeviceDao.GetDevicesByChlId(_selectChannel.Id);
                if (devices != null)
                {
                    foreach (Device dev in devices)
                    {
                        _devices.Add(dev);
                    }
                }
            }
            cbbDevice.ItemsSource = _devices;
            if (_devices.Count>0)
            {
                cbbDevice.SelectedItem = _devices[0];
            }
        }

        private void CbbDevice_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectDevice = cbbDevice.SelectedItem as Device;
            _unSelectedItems.Clear();
           
            if (_selectDevice != null)
            {
                List<DataItem> dataItems = AppManager.DataItemDao.GetDataItemsByDevId(_selectDevice.Id);
                if (dataItems != null)
                {
                    BaseDevProtocol protocol = _selectDevice.GetBaseDevProtocol();
                    if (protocol != null)
                        dataItems = protocol.DeSerializeDataItems(dataItems);
                    bool find;
                    foreach (DataItem item in dataItems)
                    {
                        find = false;
                        foreach (TransmitDataModel model in _selectedItems)
                        {
                            if (model.Id == item.Id)
                            {
                                find = true;
                                break;
                            }
                        }

                        if (!find)
                        {
                            var add = new TransmitDataModel()
                                {Id = item.Id, Name = item.Name, IsSignal = item.PhyType == PhyType.信号量};
                            if (item.TagObject is ModbusTagObject tagObject)
                            {
                                if (Math.Abs(tagObject.Coefficient) > 0)
                                    add.Radio = (int) (1 / tagObject.Coefficient);
                            }
                            _unSelectedItems.Add(add);
                        }
                           
                    }
                }

            }
           
        }

       

        private void Btn_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        #region 窗体最大化最小化关闭事件处理

        private void RightButton_CloseWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            this.Close();
        }

        private void RightButton_MaxiWindow(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        private void RightButton_MiniWindow(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        #endregion

        private void BtnSelect_OnClick(object sender, RoutedEventArgs e)
        {
            if (_unSelectedList.SelectedItems.Count == 0)
            {
                MessageBox.Show("请在左边的列表中选择数据项!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            List<TransmitDataModel> dataModels = new List<TransmitDataModel>();
            foreach (object item in _unSelectedList.SelectedItems)
            {
                if (item is TransmitDataModel model)
                {
                    dataModels.Add(model);
                }
            }

            foreach (TransmitDataModel model in dataModels)
            {
                _unSelectedItems.Remove(model);
                _selectedItems.Add(model);
                NewSelected.Add(model);
            }
        }

        private void BtnUnSelect_OnClick(object sender, RoutedEventArgs e)
        {
            if (_selectedList.SelectedItems.Count == 0)
            {
                MessageBox.Show("请在右边的列表中选择数据项!", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            List<TransmitDataModel> dataModels = new List<TransmitDataModel>();
            foreach (object item in _selectedList.SelectedItems)
            {
                if (item is TransmitDataModel model)
                {
                    dataModels.Add(model);
                }
            }

            foreach (TransmitDataModel model in dataModels)
            {
                _selectedItems.Remove(model);
                try
                {
                    NewSelected.Remove(model);
                }
                catch 
                {  
                }
            }

            CbbDevice_OnSelectionChanged(null, null);
        }

        private void BtnSelectAll_0_Click(object sender, RoutedEventArgs e)
        {
            _unSelectedList.SelectAll();
        }

        private void BtnUnSelectAll_0_Click(object sender, RoutedEventArgs e)
        {
            _unSelectedList.UnselectAll();
        }

        private void BtnSelectAll_1_Click(object sender, RoutedEventArgs e)
        {
            _selectedList.SelectAll();
        }

        private void BtnUnSelectAll_1_Click(object sender, RoutedEventArgs e)
        {
            _selectedList.UnselectAll();
        }
    }
}
