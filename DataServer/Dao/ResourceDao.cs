﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Model;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class ResourceDao : BaseDao<PageResource>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static ResourceMapper _resourceMapper;

        public ResourceDao()
        {
            if (_resourceMapper == null)
            {
                _resourceMapper = new ResourceMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_resourceMapper != null)
                return _resourceMapper.TableName;
            return "page_resource";
        }

        public PageResource GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageResource>(f => f.Id, Operator.Eq, id);
                PageResource content = result?.Connection.Get<PageResource>(predicate);
                return content;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        public List<PageResource> GetAllResources()
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                string sql = $"select Id,Name,FileExt from {GetTableName()}";
                List<PageResource> resources = result?.Connection.GetList<PageResource>(sql);
                return resources;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public bool DeleteById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageResource>(f => f.Id, Operator.Eq, id);

                return result != null && result.Connection.Delete<PageResource>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        public bool DeleteByIds(List<string> keys)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                PredicateGroup predicateGroup = new PredicateGroup { Operator = GroupOperator.Or, Predicates = new List<IPredicate>() };
                foreach (string key in keys)
                {
                    var predicate = Predicates.Field<PageResource>(f => f.Id, Operator.Eq, key);
                    predicateGroup.Predicates.Add(predicate);
                }
                return result != null && result.Connection.Delete<PageResource>(predicateGroup);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

    }
    public sealed class ResourceMapper : ClassMapper<PageResource>
    {
        public ResourceMapper()
        {
            Table("page_resource");
            Map(x => x.Id).Key(KeyType.Assigned);
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
