﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class PageInfoDao : BaseDao<PageInfo>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static PageInfoMapper _pageInfoMapper;

        public PageInfoDao()
        {
            if (_pageInfoMapper == null)
            {
                _pageInfoMapper = new PageInfoMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_pageInfoMapper != null)
                return _pageInfoMapper.TableName;
            return "page_info";
        }

        public override List<PageInfo> GetEntities()
        {
            ConnectResult result = null;
            try
            {
               
                result = OpenDbConnection();
                return result?.Connection.GetList<PageInfo>($"select Id,Name,IsHomePage,Position from {_pageInfoMapper.TableName} order by Position").ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public PageInfo GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageInfo>(f => f.Id, Operator.Eq, id);
                PageInfo pageInfo = result?.Connection.Get<PageInfo>(predicate);
                return pageInfo;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        /// <summary>
        /// 获取主页
        /// </summary>
        /// <returns></returns>
        public PageInfo GetHomePage()
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageInfo>(f => f.IsHomePage, Operator.Eq, true);
               
                PageInfo pageInfo = result?.Connection.Get<PageInfo>(predicate);
                return pageInfo;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        /// <summary>
        /// 设置主页
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public bool SetHomePage(string pageId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                result?.Connection.Update(
                    $"update {_pageInfoMapper.TableName} set IsHomePage = 0 where IsHomePage = 1");
                result?.Connection.Update(
                    $"update {_pageInfoMapper.TableName} set IsHomePage = 1 where Id= '{pageId}'");
                return true;

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public PageInfo GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageInfo>(f => f.Name, Operator.Eq, name);
                PageInfo pageInfo = result?.Connection.Get<PageInfo>(predicate);
                return pageInfo;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public bool DeleteEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<PageInfo>(f => f.Id, Operator.Eq, id);
                if (result == null) return false;
                return result.Connection.Delete<PageInfo>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
    }
    public sealed class PageInfoMapper : ClassMapper<PageInfo>
    {
        public PageInfoMapper()
        {
            Table("page_info");
            Map(x => x.Id).Key(KeyType.Assigned);
           
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
