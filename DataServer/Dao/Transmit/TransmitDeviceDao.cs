﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using Core.Model.Transmit;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao.Transmit
{
    public class TransmitDeviceDao : BaseDao<TransmitDevice>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static TransmitDeviceMapper _deviceMapper;

        public TransmitDeviceDao()
        {
            if (_deviceMapper == null)
            {
                _deviceMapper = new TransmitDeviceMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_deviceMapper != null)
                return _deviceMapper.TableName;
            return "transmit_device";
        }

        public TransmitDevice GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitDevice>(f => f.Id, Operator.Eq, id);
                IList<ISort> sort = new List<ISort>();
                sort.Add(new Sort { PropertyName = "Position", Ascending = true });
                TransmitDevice Device = result?.Connection.Get<TransmitDevice>(predicate, sort);
                return Device;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public TransmitDevice GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitDevice>(f => f.Name, Operator.Eq, name);
                TransmitDevice Device = result?.Connection.Get<TransmitDevice>(predicate);
                return Device;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public List<TransmitDevice> GetDevicesByChlId(string chlId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitDevice>(f => f.ChannelId, Operator.Eq, chlId);
                IList<ISort> sort = new List<ISort>();
                sort.Add(new Sort { PropertyName = "Position", Ascending = true });
                IEnumerable<TransmitDevice> devices = result?.Connection.GetList<TransmitDevice>(predicate, sort);
                return devices?.ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public bool DeleteDevicesByChlId(string chlId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitDevice>(f => f.ChannelId, Operator.Eq, chlId);
                return result.Connection.Delete<TransmitDevice>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

    }
    public sealed class TransmitDeviceMapper : ClassMapper<TransmitDevice>
    {
        public TransmitDeviceMapper()
        {
            Table("transmit_device");
            Map(x => x.Id).Key(KeyType.Assigned);
            Map(x => x.IsConnected).Ignore();
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
