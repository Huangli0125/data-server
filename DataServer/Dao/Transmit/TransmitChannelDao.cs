﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using Core.Model.Transmit;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao.Transmit
{
    public class TransmitChannelDao : BaseDao<TransmitChannel>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static TransmitChannelMapper _channelMapper;

        public TransmitChannelDao()
        {
            if (_channelMapper == null)
            {
                _channelMapper = new TransmitChannelMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_channelMapper != null)
                return _channelMapper.TableName;
            return "transmit_channel";
        }

        public override List<TransmitChannel> GetEntities()
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                // 并行模式下会报错 There is already an open DataReader associated with this Command which must be closed first
                // 原因是Dapper 中的协程
                IList<ISort> sort = new List<ISort>();
                sort.Add(new Sort { PropertyName = "Position", Ascending = true });
                return result?.Connection.GetAll<TransmitChannel>(sort).ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public TransmitChannel GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitChannel>(f => f.Id, Operator.Eq, id);
                TransmitChannel channel = result?.Connection.Get<TransmitChannel>(predicate);
                return channel;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public TransmitChannel GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<TransmitChannel>(f => f.Name, Operator.Eq, name);
                TransmitChannel channel = result?.Connection.Get<TransmitChannel>(predicate);
                return channel;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

       

    }
    public sealed class TransmitChannelMapper : ClassMapper<TransmitChannel>
    {
        public TransmitChannelMapper()
        {
            Table("transmit_channel");
            Map(x => x.Id).Key(KeyType.Assigned);
            Map(x => x.IsConnected).Ignore();
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
