﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Model;
using Dapper;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class RemoteItemDao : BaseDao<RemoteItem>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static RemoteItemMapper _remoteItemMapper;

        public RemoteItemDao()
        {
            if (_remoteItemMapper == null)
            {
                _remoteItemMapper = new RemoteItemMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_remoteItemMapper != null)
                return _remoteItemMapper.TableName;
            return "remote_item";
        }

        public RemoteItem GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<RemoteItem>(f => f.Id, Operator.Eq, id);
                RemoteItem item = result?.Connection.Get<RemoteItem>(predicate);
                return item;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        

        public RemoteItem GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<RemoteItem>(f => f.Name, Operator.Eq, name);
                RemoteItem item = result?.Connection.Get<RemoteItem>(predicate);
                return item;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        /// <summary>
        /// 根据遥控Id查询数据项
        /// </summary>
        /// <param name="rcId"></param>
        /// <returns></returns>
        public DataItem GetDataItemByRcId(string rcId)
        {
            ConnectResult result = null;
            try
            {
                if (string.IsNullOrEmpty(rcId)) return null;
                result = OpenDbConnection();
                string sql = $"SELECT a.* from data_item a,(SELECT * from remote_item WHERE Id='{rcId}') b WHERE a.Id = b.RelateItemId";
                List<DataItem> dataItems = result?.Connection.GetList<DataItem>(sql);
                if (dataItems != null && dataItems.Count > 0) return dataItems[0];
                return null;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public bool DeleteItemById(string rcId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<RemoteItem>(f => f.Id, Operator.Eq, rcId);
                return result.Connection.Delete<RemoteItem>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        /// <summary>
        /// 更新光字牌状态
        /// </summary>
        /// <param name="rcId"></param>
        /// <param name="on"></param>
        /// <returns></returns>
        public bool UpdateAlarmWin(string rcId, bool on)
        {
            ConnectResult result = null;
            try
            {
                if (string.IsNullOrEmpty(rcId)) return false;
                result = OpenDbConnection();
                string sql = $"UPDATE {GetTableName()} set HasAlarmWin={on} where Id='{rcId}' ";
                result?.Connection.Execute(sql);
                return true;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
    }
    public sealed class RemoteItemMapper : ClassMapper<RemoteItem>
    {
        public RemoteItemMapper()
        {
            Table("remote_item");
            Map(x => x.Id).Key(KeyType.Assigned);
           
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
