﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class ChannelDao : BaseDao<Channel>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static ChannelMapper _channelMapper;

        public ChannelDao()
        {
            if (_channelMapper == null)
            {
                _channelMapper = new ChannelMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_channelMapper != null)
                return _channelMapper.TableName;
            return "channel";
        }

        public override List<Channel> GetEntities()
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                // 并行模式下会报错 There is already an open DataReader associated with this Command which must be closed first
                // 原因是Dapper 中的协程
                IList<ISort> sort = new List<ISort>();
                sort.Add(new Sort { PropertyName = "Position", Ascending = true });
                return result?.Connection.GetAll<Channel>(sort).ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public Channel GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<Channel>(f => f.Id, Operator.Eq, id);
                Channel channel = result?.Connection.Get<Channel>(predicate);
                return channel;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public Channel GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<Channel>(f => f.Name, Operator.Eq, name);
                Channel channel = result?.Connection.Get<Channel>(predicate);
                return channel;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

       

    }
    public sealed class ChannelMapper : ClassMapper<Channel>
    {
        public ChannelMapper()
        {
            Table("channel");
            Map(x => x.Id).Key(KeyType.Assigned);
            Map(x => x.IsConnected).Ignore();
            Map(x => x.IsRunning).Ignore();
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
