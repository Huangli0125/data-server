﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class ScriptInfoDao : BaseDao<ScriptInfo>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static ScriptMapper _scriptInfoMapper;

        public ScriptInfoDao()
        {
            if (_scriptInfoMapper == null)
            {
                _scriptInfoMapper = new ScriptMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_scriptInfoMapper != null)
                return _scriptInfoMapper.TableName;
            return "script_info";
        }
        public override List<ScriptInfo> GetEntities()
        {
            ConnectResult result = null;
            try
            {

                result = OpenDbConnection();
                return result?.Connection.GetList<ScriptInfo>($"select Id,Name,PageId, from {_scriptInfoMapper.TableName}").ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public  List<ScriptInfo> GetGlobalScripts()
        {
            ConnectResult result = null;
            try
            {

                result = OpenDbConnection();
                return result?.Connection.GetList<ScriptInfo>($"select Id,Name,PageId,Content from {_scriptInfoMapper.TableName} where PageId is null").ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
       
        public ScriptInfo GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<ScriptInfo>(f => f.Id, Operator.Eq, id);
                ScriptInfo scriptInfo = result?.Connection.Get<ScriptInfo>(predicate);
                return scriptInfo;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        public ScriptInfo GetEntityByPageId(string pageId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<ScriptInfo>(f => f.PageId, Operator.Eq, pageId);
                ScriptInfo scriptInfo = result?.Connection.Get<ScriptInfo>(predicate);
                return scriptInfo;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public bool DeleteEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<ScriptInfo>(f => f.Id, Operator.Eq, id);
                if (result == null) return false;
                return result.Connection.Delete<ScriptInfo>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        public bool DeleteEntityByPageId(string pageId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<ScriptInfo>(f => f.PageId, Operator.Eq, pageId);
                if (result == null) return false;
                return result.Connection.Delete<ScriptInfo>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
    }
    public sealed class ScriptMapper : ClassMapper<ScriptInfo>
    {
        public ScriptMapper()
        {
            Table("script_info");
            Map(x => x.Id).Key(KeyType.Assigned);
           
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
