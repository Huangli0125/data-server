﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Model;
using DapperExtensions;
using DapperExtensions.Mapper;
using log4net;

namespace DataServer.Dao
{
    public class OperationDao : BaseDao<Operation>
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static OperationMapper _operationMapper;

        public OperationDao()
        {
            if (_operationMapper == null)
            {
                _operationMapper = new OperationMapper();
            }
        }

        protected override string GetTableName()
        {
            if (_operationMapper != null)
                return _operationMapper.TableName;
            return "operation";
        }

        public Operation GetEntityById(string id)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<Operation>(f => f.Id, Operator.Eq, id);
                Operation operation = result?.Connection.Get<Operation>(predicate);
                return operation;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public Operation GetEntityByName(string name)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<Operation>(f => f.Name, Operator.Eq, name);
                Operation operation = result?.Connection.Get<Operation>(predicate);
                return operation;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

        public List<Operation> GetOperationByDevType(string devTypeId)
        {
            ConnectResult result = null;
            try
            {
                if (string.IsNullOrEmpty(devTypeId))
                    return null;
                result = OpenDbConnection();
                var predicate = Predicates.Field<Operation>(f => f.DevTypeId, Operator.Eq, devTypeId);
                IEnumerable<Operation> operations = result?.Connection.GetList<Operation>(predicate);
                return operations?.ToList();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return null;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }
        public bool DeleteOperationByDevType(string devTypeId)
        {
            ConnectResult result = null;
            try
            {
                result = OpenDbConnection();
                var predicate = Predicates.Field<Operation>(f => f.DevTypeId, Operator.Eq, devTypeId);
                return result.Connection.Delete<Operation>(predicate);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
            finally
            {
                if (result != null)
                {
                    CloseDbConnection(result.Name);
                }
            }
        }

    }
    public sealed class OperationMapper : ClassMapper<Operation>
    {
        public OperationMapper()
        {
            Table("operation");
            Map(x => x.Id).Key(KeyType.Assigned);
            Map(x => x.MsgBuffer).Ignore();
            //Ignore this property entirely
            //optional, map all other columns
            AutoMap();
        }
    }
}
