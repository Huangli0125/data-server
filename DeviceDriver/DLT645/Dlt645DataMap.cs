﻿using Core.Model;
using System.Collections.Generic;

namespace DLT645
{
    public class Dlt645DataMap
    {
        public readonly static List<DataItem> SourceData = new List<DataItem>()
        {
            new DataItem("A电压",""){Address = 0x02010100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=0},
                new DataItem("B电压",""){Address = 0x02010200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=1},
                new DataItem("C电压",""){Address = 0x02010300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=2},
                new DataItem("A电流",""){Address = 0x02020100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=3},
                new DataItem("B电流",""){Address = 0x02020200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=4},
                new DataItem("C电流",""){Address = 0x02020300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=5},
                new DataItem("总有功功率",""){Address = 0x02030000, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=6},
                new DataItem("A相有功功率",""){Address = 0x02030100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=7},
                new DataItem("B相有功功率",""){Address = 0x02030200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=8},
                new DataItem("C相有功功率",""){Address = 0x02030300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=9},
                new DataItem("总无功功率","") {Address = 0x02040000, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=10},
                new DataItem("A相无功功率",""){Address = 0x02040100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=11},
                new DataItem("B相无功功率",""){Address = 0x02040200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=12},
                new DataItem("C相无功功率",""){Address = 0x02040300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=13},
                new DataItem("总视在功率",""){Address = 0x02050000, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=14},
                new DataItem("A相视在功率",""){Address = 0x02050100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=15},
                new DataItem("B相视在功率",""){Address = 0x02050200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=16},
                new DataItem("C相视在功率",""){Address = 0x02050300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=17},
                new DataItem("总功率因数","") {Address = 0x02060000, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=18},
                new DataItem("A相功率因数",""){Address = 0x02060100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=19},
                new DataItem("B相功率因数",""){Address = 0x02060200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=20},
                new DataItem("C相功率因数",""){Address = 0x02060300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=21},
                new DataItem("电网频率",""){Address = 0x02800002, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=22},
                new DataItem("当前有功需量",""){Address = 0x02800004, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=23},
                new DataItem("组合有功总电能",""){Address = 0x00000000, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=24},
                new DataItem("组合有功费率1电能",""){Address = 0x00000100, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=25},
                new DataItem("组合有功费率2电能",""){Address = 0x00000200, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=26},
                new DataItem("组合有功费率3电能",""){Address = 0x00000300, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=27},
                new DataItem("组合有功费率4电能",""){Address = 0x00000400, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=28},
                new DataItem("年月日星期",""){Address = 0x04000101, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=29},
                new DataItem("时分秒",""){Address = 0x04000102, Access = Access.只读,DataType= DataType.BCD4, PhyType = PhyType.模拟量, Position=30}
        };
 
        // 创建数据项数引用
        public static List<DataItem> GetDataItems(Device dev)
        {
            if (dev == null) return null;
            List<DataItem> dataItems = new List<DataItem>();
            foreach(var item in SourceData)
            {
                DataItem d = new DataItem(item);
                d.Name = item.Name;
                d.DeviceId = dev.Id;
                d.Address = item.Address;
                d.Position = item.Position;
                dataItems.Add(d);
            }
            return dataItems;
        }

        private Dictionary<int,DataItem> _dataItems = new Dictionary<int, DataItem>();
        public Dlt645DataMap(List<DataItem> list)
        {
            _dataItems.Clear();
            if(list!=null && list.Count > 0)
            {
                foreach(var item in list)
                {
                    _dataItems[item.Address] = item;
                }
            }
        }

        public double Ua
        {
            get 
            { 
                if (_dataItems.ContainsKey(0x02010100))
                {
                   return _dataItems[0x02010100].CurValue;
                }
                return -1; 
            }
            set
            {
                if (_dataItems.ContainsKey(0x02010100))
                {
                    _dataItems[0x02010100].CurValue = value;
                }
            }
        }
        public double Ub
        {
            get
            {
                if (_dataItems.ContainsKey(0x02010200))
                {
                    return _dataItems[0x02010200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02010200))
                {
                    _dataItems[0x02010200].CurValue = value;
                }
            }
        }
        public double Uc
        {
            get
            {
                if (_dataItems.ContainsKey(0x02010300))
                {
                    return _dataItems[0x02010300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02010300))
                {
                    _dataItems[0x02010300].CurValue = value;
                }
            }
        }

        public double Ia
        {
            get
            {
                if (_dataItems.ContainsKey(0x02020100))
                {
                    return _dataItems[0x02020100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02020100))
                {
                    _dataItems[0x02020100].CurValue = value;
                }
            }
        }

        public double Ib
        {
            get
            {
                if (_dataItems.ContainsKey(0x02020200))
                {
                    return _dataItems[0x02020200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02020200))
                {
                    _dataItems[0x02020200].CurValue = value;
                }
            }
        }
        public double Ic
        {
            get
            {
                if (_dataItems.ContainsKey(0x02020300))
                {
                    return _dataItems[0x02020300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02020300))
                {
                    _dataItems[0x02020300].CurValue = value;
                }
            }
        }
        public double Psum
        {
            get
            {
                if (_dataItems.ContainsKey(0x02030000))
                {
                    return _dataItems[0x02030000].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02030000))
                {
                    _dataItems[0x02030000].CurValue = value;
                }
            }
        }
        public double Pa
        {
            get
            {
                if (_dataItems.ContainsKey(0x02030100))
                {
                    return _dataItems[0x02030100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02030100))
                {
                    _dataItems[0x02030100].CurValue = value;
                }
            }
        }
        public double Pb
        {
            get
            {
                if (_dataItems.ContainsKey(0x02030200))
                {
                    return _dataItems[0x02030200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02030200))
                {
                    _dataItems[0x02030200].CurValue = value;
                }
            }
        }
        public double Pc
        {
            get
            {
                if (_dataItems.ContainsKey(0x02030300))
                {
                    return _dataItems[0x02030300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02030300))
                {
                    _dataItems[0x02030300].CurValue = value;
                }
            }
        }
        public double Qsum
        {
            get
            {
                if (_dataItems.ContainsKey(0x02040000))
                {
                    return _dataItems[0x02040000].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02040000))
                {
                    _dataItems[0x02040000].CurValue = value;
                }
            }
        }
        public double Qa
        {
            get
            {
                if (_dataItems.ContainsKey(0x02040100))
                {
                    return _dataItems[0x02040100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02040100))
                {
                    _dataItems[0x02040100].CurValue = value;
                }
            }
        }
        public double Qb
        {
            get
            {
                if (_dataItems.ContainsKey(0x02040200))
                {
                    return _dataItems[0x02040200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02040200))
                {
                    _dataItems[0x02040200].CurValue = value;
                }
            }
        }
        public double Qc
        {
            get
            {
                if (_dataItems.ContainsKey(0x02040300))
                {
                    return _dataItems[0x02040300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02040300))
                {
                    _dataItems[0x02040300].CurValue = value;
                }
            }
        }
        public double Ssum
        {
            get
            {
                if (_dataItems.ContainsKey(0x02050000))
                {
                    return _dataItems[0x02050000].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02050000))
                {
                    _dataItems[0x02050000].CurValue = value;
                }
            }
        }
        public double Sa
        {
            get
            {
                if (_dataItems.ContainsKey(0x02050100))
                {
                    return _dataItems[0x02050100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02050100))
                {
                    _dataItems[0x02050100].CurValue = value;
                }
            }
        }
        public double Sb
        {
            get
            {
                if (_dataItems.ContainsKey(0x02050200))
                {
                    return _dataItems[0x02050200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02050200))
                {
                    _dataItems[0x02050200].CurValue = value;
                }
            }
        }
        public double Sc
        {
            get
            {
                if (_dataItems.ContainsKey(0x02050300))
                {
                    return _dataItems[0x02050300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02050300))
                {
                    _dataItems[0x02050300].CurValue = value;
                }
            }
        }
        public double PFsum
        {
            get
            {
                if (_dataItems.ContainsKey(0x02060000))
                {
                    return _dataItems[0x02060000].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02060000))
                {
                    _dataItems[0x02060000].CurValue = value;
                }
            }
        }
        public double PFa
        {
            get
            {
                if (_dataItems.ContainsKey(0x02060100))
                {
                    return _dataItems[0x02060100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02060100))
                {
                    _dataItems[0x02060100].CurValue = value;
                }
            }
        }
        public double PFb
        {
            get
            {
                if (_dataItems.ContainsKey(0x02060200))
                {
                    return _dataItems[0x02060200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02060200))
                {
                    _dataItems[0x02060200].CurValue = value;
                }
            }
        }
        public double PFc
        {
            get
            {
                if (_dataItems.ContainsKey(0x02060300))
                {
                    return _dataItems[0x02060300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02060300))
                {
                    _dataItems[0x02060300].CurValue = value;
                }
            }
        }
        public double Freq
        {
            get
            {
                if (_dataItems.ContainsKey(0x02800002))
                {
                    return _dataItems[0x02800002].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02800002))
                {
                    _dataItems[0x02800002].CurValue = value;
                }
            }
        }
        public double Pcur
        {
            get
            {
                if (_dataItems.ContainsKey(0x02800004))
                {
                    return _dataItems[0x02800004].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x02800004))
                {
                    _dataItems[0x02800004].CurValue = value;
                }
            }
        }

        public double Cap
        {
            get
            {
                if (_dataItems.ContainsKey(0x00000000))
                {
                    return _dataItems[0x00000000].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x00000000))
                {
                    _dataItems[0x00000000].CurValue = value;
                }
            }
        }
        public double Cap1
        {
            get
            {
                if (_dataItems.ContainsKey(0x00000100))
                {
                    return _dataItems[0x00000100].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x00000100))
                {
                    _dataItems[0x00000100].CurValue = value;
                }
            }
        }
        public double Cap2
        {
            get
            {
                if (_dataItems.ContainsKey(0x00000200))
                {
                    return _dataItems[0x00000200].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x00000200))
                {
                    _dataItems[0x00000200].CurValue = value;
                }
            }
        }
        public double Cap3
        {
            get
            {
                if (_dataItems.ContainsKey(0x00000300))
                {
                    return _dataItems[0x00000300].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x00000300))
                {
                    _dataItems[0x00000300].CurValue = value;
                }
            }
        }
        public double Cap4
        {
            get
            {
                if (_dataItems.ContainsKey(0x00000400))
                {
                    return _dataItems[0x00000400].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x00000400))
                {
                    _dataItems[0x00000400].CurValue = value;
                }
            }
        }
        public double MeterDate
        {
            get
            {
                if (_dataItems.ContainsKey(0x04000101))
                {
                    return _dataItems[0x04000101].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x04000101))
                {
                    _dataItems[0x04000101].CurValue = value;
                }
            }
        }
        public double MeterTime
        {
            get
            {
                if (_dataItems.ContainsKey(0x04000102))
                {
                    return _dataItems[0x04000102].CurValue;
                }
                return -1;
            }
            set
            {
                if (_dataItems.ContainsKey(0x04000102))
                {
                    _dataItems[0x04000102].CurValue = value;
                }
            }
        }
    }
}
