﻿using Core.Msg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLT645
{
    public class dlt645Frame
    {
        public ulong  Address = 0;
        private bool _needDataId = false; // 帧中有没有数据ID （装帧的时候判断所需）
        public bool NeedDataID
        {
            get { return _needDataId; }
        }
        private UInt32 _DataID = 0;
        public UInt32 DataID
        {
            get { return _DataID; }
            set { _DataID = value; _needDataId = true; }
        }
        public byte CtrlCode;
        private bool _needPassword = false;
        public bool NeedPassword
        {
            get { return _needPassword; }
        }
        private UInt32 _passWord = 0;
        public UInt32 PassWord
        {
            get { return _passWord; }
            set { _passWord = value; _needPassword = true; }
        }
        private bool _needOperCode = false;
        public bool NeedOperCode
        {
            get { return _needOperCode; }
        }
        private UInt32 _operCode = 0;
        public UInt32 OperCode
        {
            get { return _operCode; }
            set { _operCode = value; _needOperCode = true; }
        }
        public byte[] WriteData;
        /// <summary>
        /// 数据域数据（包括数据标识码）
        /// </summary>
        public byte[] RecData;

        public dlt645Frame()
        {

        }
        public dlt645Frame(UInt64 address1, byte ctrlCode)
        {
            this.Address = address1;
            CtrlCode = ctrlCode;
        }
        public dlt645Frame(UInt64 address1, UInt32 dataId, byte ctrlCode)
        {
            this.Address = address1;
            DataID = dataId;
            CtrlCode = ctrlCode;
        }
        public dlt645Frame(UInt64 address1, byte ctrlCode, byte[] writeData)
        {
            this.Address = address1;
            WriteData = writeData;
            CtrlCode = ctrlCode;
        }
        public dlt645Frame(UInt64 address1, byte ctrlCode, UInt32 dataId, byte[] writeData)
        {
            this.Address = address1;
            DataID = dataId;
            WriteData = writeData;
            CtrlCode = ctrlCode;
        }
        public dlt645Frame(UInt64 address1, UInt32 dataId, byte ctrlCode, UInt32 passWord, byte[] writeData)
        {
            this.Address = address1;
            DataID = dataId;
            CtrlCode = ctrlCode;
            PassWord = passWord;
            WriteData = writeData;
        }
        public dlt645Frame(UInt64 address1, UInt32 dataId, byte ctrlCode, UInt32 passWord, UInt32 operCode, byte[] writeData)
        {
            this.Address = address1;
            DataID = dataId;
            CtrlCode = ctrlCode;
            PassWord = passWord;
            OperCode = operCode;
            WriteData = writeData;
        }
    }

    public sealed class Dlt645Command : RxTxCommand
    {
        private dlt645Frame _sendframe;

        /// <summary>
        /// 内部自动初始化Sendbytes  只对第一次赋值有效（第一次赋值后，再去修改的内容无法发送）
        /// </summary>
        public dlt645Frame Sendframe
        {
            get { return _sendframe; }
            set { _sendframe = value; SendBytes = Dlt645Protocol.FrameToBuffer(_sendframe); }
        }

        public Dlt645Command(string channelId,string deviceId, string strName) : base(channelId,deviceId, strName)
        {
            this.ChannelId = channelId;
            this.DeviceId = deviceId;
            Name = strName;
            NeedReply = true;
        }
    }

}
