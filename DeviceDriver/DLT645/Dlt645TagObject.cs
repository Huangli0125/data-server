﻿using Core.Model;
using Newtonsoft.Json;
using System.ComponentModel;

namespace DLT645
{

    [JsonObject(MemberSerialization.OptIn)]
    public class Dlt645TagObject : Extra
    {
        private double _coefficient = 1.0;
       
        [DisplayName("接收字节")]
        [Browsable(false)]
        public byte[] RecBytes { get; set; } = new byte[8];

        [DisplayName("倍率")]
        [JsonProperty]
        public double Coefficient
        {
            get => _coefficient;
            set
            {
                if (value == 0)
                {
                    return;
                }
                _coefficient = value;
                PointNum = 0;
                if (_coefficient < 1)  // 计算显示精度
                {
                    string strPoint = _coefficient.ToString();
                    PointNum = strPoint.Length - strPoint.IndexOf(".") - 1;
                }
            }
        }
        [DisplayName("小数位数")]
        [Browsable(false)]
        public int PointNum { get; set; } = 3;
        

    }
}
